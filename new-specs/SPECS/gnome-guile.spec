# Note that this is NOT a relocatable package
%define  nam     gnome-guile
%define  ver     0.20
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     GNOME guile interpreter
Name:        %nam
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       X11/Gnome
Source:      %{nam}-%{ver}.tar.gz
#Patch0:     gnome-guile-libglade-configure.patch
BuildRoot:   %{_tmppath}/%{name}-%{version}-root
Obsoletes:   gnome
URL:         http://www.gnome.org

%description
GNOME guile (gnomeg) is a guile interpreter with GTK and GNOME support.
A number of GNOME utilities are written to use gnomeg.

GNOME is the GNU Network Object Model Environment.  That's a fancy
name but really GNOME is a nice GUI desktop environment.  It makes
using your computer easy, powerful, and easy to configure.

%package devel
Summary:    GNOME guile libraries, includes, etc
Group:      X11/Gnome
Requires:   %{name} = %{version}

%description devel
Libraries and header files for GNOME guile development

%prep
%setup
#%patch -p0

%build

%configure --without-gtkhtml
%make

%install
%setupbuildroot

mkdir -p $RPM_BUILD_ROOT%{_datadir}/guile/site

%makeinstall \
    ROOT=$RPM_BUILD_ROOT \
    sitedir=$RPM_BUILD_ROOT%{_datadir}/guile/site \
    schemedir=$RPM_BUILD_ROOT%{_datadir}/guile install 

mkdir -p $RPM_BUILD_ROOT%{_datadir}/guile/toolkits
rm -f $RPM_BUILD_ROOT%{_datadir}/guile/toolkits/libgtkstubs.so
ln -s %{_libdir}/libguilegtk.so \
      $RPM_BUILD_ROOT%{_datadir}/guile/toolkits/libgtkstubs.so

# for some reason this didn't get installed by makeinstall
# --jurgen@botz.org, 4/7/2001
cp guile-gtk/gtk/event-repl.scm $RPM_BUILD_ROOT%{_datadir}/guile/site/event-repl.scm

%clean
%cleanbuildroot

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%{_libdir}/lib*.so.*

%{_datadir}/guile/gtk/*.scm
%{_datadir}/guile/gtk-1.2/*.scm
%{_datadir}/guile/gnome/*.scm
%{_datadir}/guile/toolkits/libgtkstubs.so
%{_datadir}/guile/site/event-repl.scm

%files devel
%attr(-, root, root) %{_libdir}/lib*.so
%attr(-, root, root) %{_libdir}/*a
%attr(-, root, root) %{_includedir}/*
%attr(-, root, root) %{_datadir}/guile-gtk/*.defs


%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixed up macros, and removed hard-coded paths.

* Sat Feb 27 1999 Gregory Mclean <gregm@comstar.net>
- fixed this up so it will build on more then one alpha and this file is
  generated.

* Wed May 27 1998 Michael Fulbright <msf@redhat.com>
- modified file list to include %{_datadir}/guile, %{_datadir}/gtk

* Fri Mar 13 1998 Marc Ewing <marc@redhat.com>
- Integrate into gnome-guile CVS source tree
