%define  ver     0.6.2
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     Gtk+ GUI builder
Name:        glade
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       Development/Tools
Source:      glade-%{ver}.tar.gz
BuildRoot:   %{_tmppath}/glade-%{ver}-root
URL:         http://www.gnome.org/

%description
Glade is a GUI builder for Gtk.

%prep
%setup

%build

%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS COPYING ChangeLog NEWS README

%{_bindir}/glade
%{_datadir}/locale/*/*/*.mo
%{_datadir}/glade
%{_datadir}/gnome/apps/Development/glade.desktop
%{_datadir}/pixmaps/*
%{_datadir}/gnome/help/glade

%changelog
* Wed Feb 21 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- macro fixes, removal of hard-coded paths

* Thu Jul 22 1999 Herbert Valerio Riedel <hvr@hvrlab.dhs.org>
- changed configure options in order to build on all alphas

* Wed Jun 23 1999 Jose Mercado <jmercado@mit.edu>
- Changed the Source variable to use %{var}.
- Fixed glade.desktop's path so rpm will find it.
