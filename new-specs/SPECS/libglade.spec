# Note that this is NOT a relocatable package
%define name	libglade
%define ver	0.16
%define RELEASE 0_plain_0
%define rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:    libglade library
Name:       %name
Version:    %ver
Release:    %rel
Copyright:  LGPL
Group:      X11/Libraries
Source:     libglade-%{ver}.tar.gz
BuildRoot:  %{_tmppath}/%{name}-root
Packager:   James Henstridge
URL:        http://www.gnome.org
Requires:   gtk+
Requires:   libxml >= 1.3

%description
This library allows you to load user interfaces in your program, which are
stored externally.  This allows alteration of the interface without
recompilation of the program.

The interfaces can also be edited with GLADE.

%package devel
Summary:   Libraries, includes, etc to develop libglade applications
Group:     X11/libraries
Requires:  %{name} = %{version}
Requires:  gtk+-devel libxml-devel

%description devel
Libraries, include files, etc you can use to develop libglade applications.

%prep
%setup

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS ChangeLog NEWS README COPYING
%{_libdir}/lib*.so.*

%files devel
%defattr(-, %fowner, %fgroup)
%doc test-libglade.c
%doc *.glade
%doc %{_datadir}/gnome/html/libglade/*
%{_bindir}/*
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/lib*.la
%{_includedir}/glade/*
%{_datadir}/aclocal/*
%{_libdir}/libgladeConf.sh
%{_libdir}/pkgconfig/*.pc


%changelog
* Wed Feb 21 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixed hard-coded paths and macros.

* Sun Nov  1 1998 James Henstridge <james@daa.com.au>
- Updated the dependencies of the devel package, so users must have gtk+-devel.

* Sun Oct 25 1998 James Henstridge <james@daa.com.au>
- Initial release 0.0.1
