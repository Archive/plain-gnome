# Note this is NOT a relocatable thing :)
%define ver		0.2
%define RELEASE		0_plain_0
%define rel		%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:    ScrollKeeper is a cataloging system for documentation on open systems
Name:       scrollkeeper
Version:    %ver
Release:    %rel
Source0:    %{name}-%{ver}.tar.gz
Copyright:  LGPL
Group:      System Environment/Base
BuildRoot:  %{_tmppath}/%{name}-buildroot
URL:        http://scrollkeeper.sourceforge.net/
Requires:   libxml


%description 
ScrollKeeper is a cataloging system for documentation. It manages
documentation metadata (as specified by the Open Source Metadata
Framework (OMF)) and provides a simple API to allow help browsers to
find, sort, and search the document catalog. It will also be able to
communicate with catalog servers on the Net to search for documents
which are not on the local system.

%prep
%setup

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

function LinkLang() {
    src=$1; dst=$2
    rm -rf ${RPM_BUILD_ROOT}%{_datadir}/scrollkeeper/Templates/$dst
    ln -sf %{_datadir}/scrollkeeper/Templates/$src ${RPM_BUILD_ROOT}%{_datadir}/scrollkeeper/Templates/$dst
}
LinkLang bg bg_BG
LinkLang de de_AT
LinkLang C en
LinkLang en en_AU
LinkLang en en_GB
LinkLang en en_SE
LinkLang en en_UK
LinkLang en en_US
LinkLang es es_DO
LinkLang es es_ES
LinkLang es es_GT
LinkLang es es_HN
LinkLang es es_MX
LinkLang es es_PA
LinkLang es es_PE
LinkLang es es_SV
LinkLang ja ja_JP.eucJP
LinkLang nb no
LinkLang nn no_NY
LinkLang pt pt_BR
LinkLang pt pt_PT
LinkLang sr sr_YU
LinkLang sv sv_SE
LinkLang zh zh_CN
LinkLang zh zh_CN.GB2312
LinkLang zh zh_TW
LinkLang zh zh_TW.Big5

%clean
%cleanbuildroot


%files
%defattr(-,%fowner,%fgroup)
%doc COPYING COPYING-DOCS AUTHORS README ChangeLog NEWS INSTALL
%doc -P doc/scrollkeeper_manual/C/*.sgml
%{_datadir}/omf/*
%{_bindir}/*
%{_libdir}/*
%{_mandir}/man8/*
%{_datadir}/scrollkeeper
%{_datadir}/locale/*/*

%post
if [ $1 = 1 ]; then
  # There was previously no SK installed.
  # ie. make a new %{_localstatedir}/lib/scrollkeeper.
  mkdir -p %{_localstatedir}/lib/scrollkeeper
  scrollkeeper-update -p %{_localstatedir}/lib/scrollkeeper
fi
if [ $1 = 2 ]; then
  # There was previously a SK installed.
  #  ie. don't make a new %{_localstatedir}/lib/scrollkeeper.
  # However, version 0.0.4 of SK did not properly create this
  # directory if a previous SK was installed, so just to be sure...
  rm -rf %{_localstatedir}/lib/scrollkeeper
  mkdir -p %{_localstatedir}/lib/scrollkeeper
  scrollkeeper-update -p %{_localstatedir}/lib/scrollkeeper
fi

%postun
if [ $1 = 0 ]; then
  # SK is being removed, not upgraded.  
  #  ie. erase {localstatedir}/lib/scrollkeeper.
  rm -rf %{_localstatedir}/lib/scrollkeeper
fi
#if [ $1 = 1 ]; then
#  # SK is being upgraded.  Do not erase {localstatedir}/lib/scrollkeeper.
#fi

%changelog
* Tue Feb 15 2001 Dan Mueth <dan@eazel.com>
- added line to include the translations .mo file

* Tue Feb 06 2001 Dan Mueth <dan@eazel.com>
- fixed up pre and post installation scripts

* Tue Feb 06 2001 Laszlo Kovacs <laszlo.kovacs@sun.com>
- added all the locale directories and links for the template
  content list files

* Wed Jan 17 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- converted to scrollkeeper.spec.in

* Sat Dec 16 2000 Laszlo Kovacs <laszlo.kovacs@sun.com>
- help files added

* Fri Dec 8 2000 Laszlo Kovacs <laszlo.kovacs@sun.com>
- various small fixes added

* Thu Dec 7 2000 Laszlo Kovacs <laszlo.kovacs@sun.com>
- fixing localstatedir problem
- adding postinstall and postuninstall scripts

* Tue Dec 5 2000 Gregory Leblanc <gleblanc@cu-portland.edu>
- adding COPYING, AUTHORS, etc
- fixed localstatedir for the OMF files

* Fri Nov 10 2000 Gregory Leblanc <gleblanc@cu-portland.edu>
- Initial spec file created.


