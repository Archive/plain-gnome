%define  ver     1.1.15
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary: 	A C++ interface for Gnome libs (a GUI library for X).
Name: 		gnomemm
Version: 	%ver
Release: 	%rel
Copyright: 	LGPL
Group: 		System Environment/Libraries
Source: 	%{name}-%{version}.tar.gz
#Patch0:         gnomemm-conf-sh-location.patch
URL: 		http://gtkmm.sourceforge.net/
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
Requires:	gnome-libs, gtkmm
BuildRequires:  gtkmm-devel, libsigc++-devel

%description
This package provides a C++ interface for GnomeUI.  It is a subpackage
of the Gtk-- project.  The interface provides a convenient interface for C++
programmers to create Gnome GUIs with GTK+'s flexible object-oriented 
framework.

%package	devel
Summary: 	Headers for developing programs that will use Gnome--.
Group: 		Development/Libraries
Requires:       %{name}, gnome-libs-devel

%description    devel
This package contains the headers that programmers will need to develop
applications which will use Gnome--, part of Gtk-- the C++ interface to 
the GTK+ (the Gimp ToolKit) GUI library.

%prep
%setup
#%patch -p1

%build
%configure --disable-maintainer-mode \
           --disable-static \
           --enable-shared --enable-docs
%make

%install
%setupbuildroot
%makeinstall

# replace examples.conf by a really simple one
echo 'CXXBUILD = g++ -O2 $< -o $@ `gtkmm-config --cflags --libs` ' \
	> examples/examples.conf

# strip down the docs 
find docs/ \
\( 	-name 'Makefile' -or	\
	-name 'Makefile.in' -or	\
	-name 'Makefile.am' -or	\
	-name '*.m4' -or	\
	-name 'html' -or	\
	-name 'header' -or 	\
	-name '*.h' 		\
\)	-exec rm {} \;

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
%cleanbuildroot

%files
%defattr(-, %fowner, %fgroup)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/*.so.*

%files  devel
%defattr(-, %fowner, %fgroup)
%doc examples/ docs/  AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_includedir}/*.h
%{_includedir}/gnome--
%{_libdir}/*.la
%{_libdir}/*.so

# there doesn't seem to be anything installed in %{_bindir}
# also we're using --disable-static above, so there are no *.a's
# --jurgen@botz.org 4/7/2001
#%{_bindir}/*
#%{_libdir}/*.a
#%{_libdir}/*.sh

###########################################################################
%changelog
* Thu Mar 01 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- removed fixed paths, and updated with better macros.

* Thu May 11 2000 Herbert Valerio Riedel <hvr@gnu.org>
- removed lib/gtkmm from files section
- removed empty obsolete tags

* Sun Jan 30 2000 Karl Einar Nelson <kenelson@sourceforge.net>
- adapted from gtk--.spec
