# Note that this is NOT a relocatable package
%define  ver     0.10.1
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

# libexecdir is overridden in one of the Makefiles, we need to set it to
# the same thing here
%define _libexecdir    %{_libdir}/%{name}/loaders

Name:        gdk-pixbuf
Summary:     The GdkPixBuf image handling library
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       System Environment/Libraries
Source:      %{name}-%{ver}.tar.gz
URL:         http://www.gnome.org/
BuildRoot:   %{_tmppath}/%{name}-root
#Requires:   gtk+ >= 1.2

%description
The GdkPixBuf library provides a number of features, including :

- GdkPixbuf structure for representing images.
- Image loading facilities.
- Rendering of a GdkPixBuf into various formats:
  drawables (windows, pixmaps), GdkRGB buffers.
- Fast scaling and compositing of pixbufs.
- Simple animation loading (ie. animated gifs)

In addition, this module also provides a little libgnomecanvaspixbuf
library, which contains a GNOME Canvas item to display pixbufs with
full affine transformations.

%package devel
Summary:    Libraries and include files for developing GdkPixBuf applications.
Group:      Development/Libraries
Requires:   %name = %ver
#Obsoletes: %name-devel

%description devel
Libraries and include files for developing GdkPixBuf applications.

%prep
%setup -q

%build

%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
#%cleanbuildroot


%post 
if ! grep %{_libdir} %{_sysconfdir}/ld.so.conf >/dev/null 2>&1 ; then
  echo "%{_libdir}" >> %{_sysconfdir}/ld.so.conf
fi

/sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README TODO doc/*.txt doc/html
%{_libdir}/lib*.so.*
%{_libexecdir}/lib*.so*

%files devel
%defattr(-, %fowner, %fgroup)
%{_bindir}/*
%{_libdir}/lib*.so
%{_libdir}/*.a
%{_libdir}/*.la
%{_libexecdir}/lib*.a
%{_libexecdir}/lib*.la
%{_libdir}/*.sh
%{_includedir}/*
%{_datadir}/aclocal/*
%{_datadir}/gnome/html/*


%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- A few fixups, and better macros

* Sat Jan 22 2000 Ross Golder <rossigee@bigfoot.com>
- Borrowed from gnome-libs to integrate into gdk-pixbuf source tree
