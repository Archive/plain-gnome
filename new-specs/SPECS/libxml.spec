# Note that this is NOT a relocatable package
%define ver      1.8.11
%define RELEASE  0_plain_0
%define rel      %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:      libXML library
Name:         libxml
Version:      %ver
Release:      %rel
Copyright:    LGPL
Group:        Development/Libraries
Source:       libxml-%{ver}.tar.gz
BuildRoot:    %{_tmppath}/libxml-%{PACKAGE_VERSION}-root
URL:          http://rpmfind.net/veillard/XML/
Prereq:       /sbin/install-info

%description
This library allows you to manipulate XML files.

%package devel
Summary: Libraries, includes, etc to develop libxml applications
Group: Development/Libraries
Requires: libxml = %{version}

%description devel
Libraries, include files, etc you can use to develop libxml applications.

%prep
%setup

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

# hack to get libxml.so.0 too !
# Get rid of it once deps to libxml.so.0 have disapeared.
#
#if [ -f $RPM_BUILD_ROOT/%{_libdir}/libxml.so.1.8.11 ]
#then
#   (cd $RPM_BUILD_ROOT/%{_libdir}/ ; cp libxml.so.1.8.11 libxml.so.0.99.0 ; ln -sf libxml.so.0.99.0 libxml.so.0)
#fi
#
# another hack to get /usr/include/gnome-xml/libxml/
#
if [ -d $RPM_BUILD_ROOT/%{_includedir}/gnome-xml ]
then
    (cd $RPM_BUILD_ROOT/%{_includedir}/gnome-xml ; ln -sf . libxml)
fi

%clean
%cleanbuildroot

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)
%doc AUTHORS ChangeLog NEWS README COPYING COPYING.LIB TODO
%{_libdir}/lib*.so.*

%files devel
%defattr(-, %fowner, %fgroup)
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/lib*.la
%{_libdir}/*.sh
%{_includedir}/*
%{_bindir}/*
%{_datadir}/gnome-xml

%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixed macros, removed hard-coded paths, that sort of thing.

* Thu Sep 23 1999 Daniel Veillard <Daniel.Veillard@w3.org>
- corrected the spec file alpha stuff
- switched to version 1.7.1
- Added validation, XPath, nanohttp, removed memory leaks
- Renamed CHAR to xmlChar

* Wed Jun  2 1999 Daniel Veillard <Daniel.Veillard@w3.org>
- Switched to version 1.1: SAX extensions, better entities support, lots of
  bug fixes.

* Sun Oct  4 1998 Daniel Veillard <Daniel.Veillard@w3.org>
- Added xml-config to the package

* Thu Sep 24 1998 Michael Fulbright <msf@redhat.com>
- Built release 0.30
