%define name		ammonite
%define ver		1.0.0
%define RELEASE		0_plain_0
%define rel		%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Name:		%name
Summary:	Ammonite is a portion of the Eazel Services authentication framework.
Version: 	%ver
Release: 	%rel
Copyright: 	GPL
Group:		User Interface/Desktop
Source: 	%{name}-%{version}.tar.gz

URL: 		http://nautilus.eazel.com/
BuildRoot:	%{_tmppath}/%{name}-%{ver}-root
BuildRequires:  xml-i18n-tools
BuildRequires:  openssl-devel
Requires:	gnome-vfs >= 0.3.1
#Requires:	openssl
# Requires:	openssl >= 0.9.5
# it really does require that version, but we would rather have
# ammonite break than break a users ssh so we are putting no version
# requirement in so we can distribute and rsa free version

%description 
Ammonite is a non-caching client-side HTTP proxy with a
set of special features required by Eazel to communicate with Eazel
Services.  Ammonite provides the user authentication and encryption
features used by Eazel Services.  It is part of the GNOME project, and
its source code can be found in the GNOME CVS repository.

You will need to install Ammonite if you intend to use Eazel Services.

Ammonite links statically with OpenSSL, which is (C) Eric Young.

Ammonite was originally based on TinyProxy v1.3, written by by Steven Young 
<sdyoung@well.com> and Robert James Kaes <rjkaes@flarenet.com>.

%package devel
Summary:	Libraries and include files for developing Ammonite clients
Group:		Development/Libraries
Requires:	%name = %{PACKAGE_VERSION}
Obsoletes:	%{name}-devel

%description devel
This package provides the necessary development libraries and include
files to allow you to develop components that make use of the Ammonite authentication
services.  You will need to install this package if you intend to build Nautilus
from source code.

%prep
%setup

%build
LC_ALL=""
LINGUAS=""
LANG=""
export LC_ALL LINGUAS LANG

%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%post
if ! grep %{_libdir} /etc/ld.so.conf > /dev/null ; then
  echo "%{_libdir}" >> /etc/ld.so.conf
fi
  
/sbin/ldconfig
  
%postun -p /sbin/ldconfig

%files

%defattr(0555, %fowner, %fgroup)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%{_libdir}/*.sh

%defattr (0444, %fowner, %fgroup)
%config %{_sysconfdir}/vfs/modules/*.conf
%{_datadir}/oaf/*.oaf
%{_datadir}/nautilus/certs/*.pem
%{_datadir}/idl/*.idl
%{_datadir}/pixmaps/nautilus/*.png

%files devel

%defattr(0555, %fowner, %fgroup)
%{_libdir}/*.a

%defattr(0444, %fowner, %fgroup)
%{_includedir}/libtrilobite/*.h


%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- updates and fixes for bad macros, and hard-coded paths

* Wed Apr 26 2000 Robin * Slomkowski <rsllomkow@eazel.com>
- created this thing

* Wed Apr 26 2000 Mike Fleming <mfleming@eazel.com>
- Small edits of description fields
