# Note that this is NOT a relocatable package
%define  ver     1.0.12
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     LibGTop library
Name:        libgtop
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       X11/Libraries
Source:      libgtop-%{ver}.tar.gz
BuildRoot:   %{_tmppath}/libgtop-%{ver}-root
URL:         http://www.home-of-linux.org/gnome/libgtop/
Prereq:      /sbin/install-info

%description
A library that fetches information about the running system such as
cpu and memory usage, active processes etc.

On Linux systems, these information are taken directly from the /proc
filesystem while on other systems a server is used to read those
information from /dev/kmem or whatever. 

%package devel
Summary:  Libraries, includes, etc to develop LibGTop applications
Group:    X11/libraries
Requires: %{name} = %{version}

%description devel
Libraries, include files, etc you can use to develop GNOME applications.

%package examples
Summary:  Examples for LibGTop
Group:    X11/libraries
Requires: %{name} = %{version}

%description examples
Examples for LibGTop.

%prep
%setup

%build

%configure \
    --without-linux-table --with-libgtop-inodedb \
    --with-libgtop-examples --with-libgtop-guile --with-libgtop-smp 
%make

%install
%setupbuildroot

%makeinstall

# why?  --jurgen@botz.org
rm -fr $RPM_BUILD_ROOT/%{_includedir}/libgtop

%clean
%cleanbuildroot

%post -p /sbin/ldconfig
/sbin/install-info %{_infodir}/libgtop.info.gz %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/libgtop.info.gz %{_infodir}/dir
fi

%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)
%doc RELNOTES-0.25 RELNOTES-1.0 AUTHORS ChangeLog NEWS README
%doc copyright.txt
%doc src/inodedb/README.inodedb
%{_bindir}/*
%{_libexecdir}/libgtop
%{_libdir}/lib*.so.*
%{_datadir}/locale/*/LC_MESSAGES/*
%{_infodir}/libgtop*

%files devel
%defattr(-, %fowner, %fgroup)
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/lib*.la
%{_libdir}/*.sh
%{_libdir}/*.def
%{_includedir}/*

%files examples
%defattr(-,%fowner,%fgroup)
%{_libexecdir}/libgtop


%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- better macros, and general fixups

* Tue Aug 19 1998 Martin Baulig <martin@home-of-linux.org>
- released LibGTop 0.25.0

* Sun Aug 16 1998 Martin Baulig <martin@home-of-linux.org>
- first version of the RPM
