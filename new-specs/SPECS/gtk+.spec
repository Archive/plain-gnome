# Note that this is NOT a relocatable package
%define  ver     1.2.9
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     The Gimp Toolkit
Name:        gtk+
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       X11/Libraries
Source:      gtk+-%{ver}.tar.gz
BuildRoot:   %{_tmppath}/gtk-%{PACKAGE_VERSION}-root
Obsoletes:   gtk
URL:         http://www.gtk.org
Prereq:      /sbin/install-info
Requires:    glib

%description
The X libraries originally written for the GIMP, which are now used by
several other programs as well.

%package devel
Summary:    GIMP Toolkit and GIMP Drawing Kit
Group:      X11/Libraries
Requires:   %{name} = %{version}
Obsoletes:  gtk-devel
PreReq:     /sbin/install-info

%description devel
Static libraries and header files for the GIMP's X libraries, which are
available as public libraries.  GLIB includes generally useful data
structures, GDK is a drawing toolkit which provides a thin layer over
Xlib to help automate things like dealing with different color depths,
and GTK is a widget set for creating user interfaces.
  
%prep
%setup

%build

%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/gdk.info{,.gz} %{_infodir}/dir
/sbin/install-info %{_infodir}/gtk.info{,.gz} %{_infodir}/dir

%preun devel
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/gdk.info{,.gz} %{_infodir}/dir
    /sbin/install-info --delete %{_infodir}/gtk.info{,.gz} %{_infodir}/dir
fi

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_libdir}/libgtk-1.2.so.*
%{_libdir}/libgdk-1.2.so.*
%{_datadir}/themes/Default
%{_datadir}/locale/*/*/*
%{_libdir}/pkgconfig/*
%{_sysconfdir}/gtk

%files devel
%defattr(-, %fowner, %fgroup)

%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/lib*.la
%{_includedir}/*
%{_infodir}/*.info*
%{_mandir}/man1/*
%{_datadir}/aclocal/*
%{_bindir}/*


%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- updates, fixes for hard-coded paths, and better macros

* Sun Feb 21 1999 Michael Fulbright <drmike@redhat.com>
- updated spec file

* Sun Oct 25 1998 Shawn T. Amundson <amundson@gtk.org>
- Fixed Source: to point to v1.1 

* Tue Aug 04 1998 Michael Fulbright <msf@redhat.com>
- change %postun to %preun

* Mon Jun 27 1998 Shawn T. Amundson <unknown@unknown.org>
- Changed version to 1.1.0

* Thu Jun 11 1998 Dick Porter <dick@cymru.net>
- Removed glib, since it is its own module now

* Mon Apr 13 1998 Marc Ewing <marc@redhat.com>
- Split out glib package

* Tue Apr  8 1998 Shawn T. Amundson <amundson@gtk.org>
- Changed version to 1.0.0

* Tue Apr  7 1998 Owen Taylor <otaylor@gtk.org>
- Changed version to 0.99.10

* Thu Mar 19 1998 Shawn T. Amundson <amundson@gimp.org>
- Changed version to 0.99.9
- Changed gtk home page to www.gtk.org

* Thu Mar 19 1998 Shawn T. Amundson <amundson@gimp.org>
- Changed version to 0.99.8

* Sun Mar 15 1998 Marc Ewing <marc@redhat.com>
- Added aclocal and bin stuff to file list.
- Added -k to the SMP make line.
- Added lib/glib to file list.

* Fri Mar 14 1998 Shawn T. Amundson <amundson@gimp.org>
- Changed version to 0.99.7

* Fri Mar 14 1998 Shawn T. Amundson <amundson@gimp.org>
- Updated ftp url and changed version to 0.99.6

* Thu Mar 12 1998 Marc Ewing <marc@redhat.com>
- Reworked to integrate into gtk+ source tree
- Truncated ChangeLog.  Previous Authors:
  Trond Eivind Glomsrod <teg@pvv.ntnu.no>
  Michael K. Johnson <johnsonm@redhat.com>
  Otto Hammersmith <otto@redhat.com>
