%define  ver     1.2
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     GNOME binary editor
Name:        ghex
Version:     %{ver}
Release:     %{rel}
Group:       Applications/Editors
Copyright:   GPL
Url:         "http://pluton.ijs.si/~jaka/gnome.html#GHEX"
Source:      ghex-%{version}.tar.gz
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root

%description
GHex allows the user to load data from any file, view and edit it in either
hex or ascii. A must for anyone playing games that use non-ascii format for
saving.

%prep

%setup

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%files
%defattr(-,%fowner,%fgroup)
%doc README COPYING AUTHORS
%attr(755,root,root) %{_bindir}/*
%{_datadir}/gnome/apps/Applications/*
%{_datadir}/gnome/help/ghex/*/*
%{_datadir}/pixmaps/*
%{_datadir}/locale/*/*/*
%{_datadir}/omf/ghex


%changelog
* Wed Feb 21 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- removed hard-coded paths, updated macros.

* Sun Oct 22 2000 John Gotts <jgotts@linuxsavvy.com>
- Minor modifications.

* Sun May 14 2000 John Gotts <jgotts@linuxsavvy.com>
- New SPEC file.
