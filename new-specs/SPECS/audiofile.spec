%define  ver     0.2.1
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     Library to handle various audio file formats
Name:        audiofile
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       Libraries/Sound
Source:      audiofile-%{version}.tar.gz
BuildRoot:   %{_tmppath}/audiofile-%{version}-root
Obsoletes:   libaudiofile

%description
Library to handle various audio file formats.
Used by the esound daemon.

%package devel
Summary: Libraries, includes, etc to develop audiofile applications
Group: Libraries

%description devel
Libraries, include files, etc you can use to develop audiofile applications.

%prep
%setup

%build

%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%files
%defattr(-, %fowner, %fgroup)
%doc COPYING TODO README ChangeLog docs
%{_bindir}/*
%{_libdir}/lib*.so.*

%files devel
%defattr(-, %fowner, %fgroup)
%{_libdir}/lib*.so
%{_libdir}/*.a
%{_libdir}/*.la
%{_includedir}/*
%{_datadir}/aclocal/*

%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- Updated and fixed macros

* Fri Nov 20 1998 Michael Fulbright <drmike@redhat.com>
- First try at a spec file
