%define ver	1.4.1
%define RELEASE	0_plain_0
%define rel	%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:        GNOME User Documentation
Name:           gnome-user-docs
Version:        %ver
Release:        %rel
Copyright:      FDL
Distribution:   GNOME RPMS
Source:         gnome-user-docs-%{version}.tar.gz
Group:          Documentation
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-root

%description
This package contains the GNOME Glossary, Introduction to GNOME, and a Unix
Primer.

%prep
%setup

%build
%configure
%make 

%install
%setupbuildroot
%makeinstall

%clean 
%cleanbuildroot

%post
which scrollkeeper-update > /dev/null 2>&1 && scrollkeeper-update
exit 0

%postun
which scrollkeeper-update > /dev/null 2>&1 && scrollkeeper-update
exit 0

%files
%defattr(-, %fowner, %fgroup)
%{_prefix}/*

%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- de-uglification, and fixed the macros.

* Mon Nov 27 2000 Kenny Graunke <kwg@teleport.com>
- Initial cut
