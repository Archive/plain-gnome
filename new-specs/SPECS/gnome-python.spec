%define ver         1.4.0
%define pygnome_ver %ver
%define py_ver      1.5
%define pygtk_ver   0.6.7
%define RELEASE     0_plain_0
%define rel         %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     The sources for the PyGTK and PyGNOME Python extension modules.
Name:        gnome-python
Version:     %ver
Release:     %rel
Source:      gnome-python-%{pygnome_ver}.tar.gz
Copyright:   LGPL
Group:       System Environment/Libraries
BuildRoot:   %{_tmppath}/gnome-python-root
Requires:    gtk+ >= 1.2.6
Requires:    gnome-libs >= 1.2.0
Requires:    pygtk = %{pygtk_ver}

%description
The gnome-python package contains the source packages for the Python
bindings for GTK+ and GNOME (PyGTK and PyGNOME, respectively). 

PyGTK is an extension module for Python that provides access to the
GTK+ widget set. Just about anything (within reason) you can write in
C with GTK+, you can write in Python with PyGTK, but with all of
Python's benefits.

PyGNOME is an extension module for Python that provides access to the
base GNOME libraries, so you have access to more widgets, a simple
configuration interface, and metadata support.

%package -n pygtk
Version:    %{pygtk_ver}
Summary:    Python bindings for the GTK+ widget set.
Group:      Development/Languages
Requires:   glib, imlib, python >= 1.5.2
Requires:   gtk+ >= 1.2.6

%description -n pygtk
PyGTK is an extension module for Python that gives you access to the
GTK+ widget set.  Just about anything you can write in C with GTK+ you
can write in Python with PyGTK (within reason), but with all of
Python's benefits. PyGTK provides an object-oriented interface at a
slightly higher level than the C interface. The PyGTK interface does
all of the type casting and reference counting that you'd have to do
yourself using the C API.

Install pygtk if you need Python bindings for the GTK+ widget set.

%package -n pygtk-libglade
Version:    %{pygtk_ver}
Summary:    A wrapper for the libglade library for use with PyGTK
Group:      Development/Languages
Requires:   pygtk = %{pygtk_ver}

%description -n pygtk-libglade
This module contains a wrapper for the libglade library.  Libglade is a
library similar to the pyglade module, except that it is written in C (so
is faster) and is more complete.

%package -n pygnome-libglade
Version:    %{pygnome_ver}
Summary:    GNOME support for the libglade python wrapper
Group:      Development/Languages
Requires:   pygnome = %{pygnome_ver}
Requires:   pygtk-libglade = %{pygtk_ver}

%description -n pygnome-libglade
This module contains GNOME support to suppliment the libglade python
wrapper.  Libglade is a library similar to the pyglade module, except
that it is written in C (so is faster) and is more complete.

%package -n pygnome
Version:    %{pygnome_ver}
Summary:    Python bindings for the GNOME libraries.
Group:      Development/Languages
Requires:   pygtk = %{pygtk_ver}
Requires:   gnome-libs

%description -n pygnome
PyGNOME is an extension module for python that gives you access to the
base GNOME libraries.  This means you have access to more widgets, simple
configuration interface, metadata support and many other features.

Install pygnome if you need Python bindings for the GNOME libraries.

%package -n pygnome-applet
Version:    %{pygnome_ver}
Summary:    Python bindings for GNOME Panel applets.
Group:      Development/Languages
Requires:   pygnome = %{pygnome_ver}

%description -n pygnome-applet
This module contains a wrapper that allows GNOME Panel applets to be
written in Python.

%package -n pygnome-capplet
Version:    %{pygnome_ver}
Summary:    Python bindings for GNOME Panel applets.
Group:      Development/Languages
Requires:   pygnome = %{pygnome_ver}

%description -n pygnome-capplet
This module contains a wrapper that allows GNOME Control Center
capplets to be in Python.

%prep
%setup

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%files -n pygtk
%doc NEWS README MAPPING AUTHORS examples ChangeLog description.py
%{_libdir}/python%{py_ver}/site-packages/gtk.py*
%{_libdir}/python%{py_ver}/site-packages/Gtkinter.py*
%{_libdir}/python%{py_ver}/site-packages/GtkExtra.py*
%{_libdir}/python%{py_ver}/site-packages/GTK.py*
%{_libdir}/python%{py_ver}/site-packages/GDK.py*
%{_libdir}/python%{py_ver}/site-packages/GdkImlib.py*
%{_libdir}/python%{py_ver}/site-packages/pyglade/*.py*

%{_libdir}/python%{py_ver}/site-packages/_gtkmodule.so
%{_libdir}/python%{py_ver}/site-packages/_gdkimlibmodule.so

%{_includedir}/pygtk

%doc pygtk/AUTHORS pygtk/NEWS pygtk/README pygtk/MAPPING pygtk/ChangeLog
%doc pygtk/description.py pygtk/examples

%files -n pygtk-libglade
%{_libdir}/python%{py_ver}/site-packages/libglade.py*
%{_libdir}/python%{py_ver}/site-packages/_libglademodule.so

%files -n pygnome-libglade
%{_libdir}/python%{py_ver}/site-packages/_gladegnomemodule.so

# the following are only included if gnome-core and control-center were
# installed during the build
%files -n pygnome-applet
%{_libdir}/python%{py_ver}/site-packages/_appletmodule.so
%{_libdir}/python%{py_ver}/site-packages/gnome/applet.py*

%files -n pygnome-capplet
%{_libdir}/python%{py_ver}/site-packages/_cappletmodule.so
%{_libdir}/python%{py_ver}/site-packages/gnome/capplet.py*

%files -n pygnome
%{_libdir}/python%{py_ver}/site-packages/gettext.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/__init__.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/affine.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/config.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/file.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/help.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/history.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/metadata.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/mime.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/score.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/triggers.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/ui.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/uiconsts.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/url.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/xmhtml.py*
%{_libdir}/python%{py_ver}/site-packages/gnome/zvt.py*

%{_libdir}/python%{py_ver}/site-packages/_gnomemodule.so
%{_libdir}/python%{py_ver}/site-packages/_gnomeuimodule.so
%{_libdir}/python%{py_ver}/site-packages/_zvtmodule.so
%{_libdir}/python%{py_ver}/site-packages/_gtkxmhtmlmodule.so

%doc AUTHORS NEWS README ChangeLog
%doc pygnome/examples

%changelog
* Fri Jan 21 2000 Matt Wilson <msw@redhat.com>
- added pygnome-libglade subpackage

* Wed Jan  5 2000 Matt Wilson <msw@redhat.com>
- split applet and capplet modules into their own package
