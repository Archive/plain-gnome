# Note that this is NOT a relocatable package
%define name		nautilus
%define ver		1.0.1
%define RELEASE		0_plain_0
%define rel		%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

# these aren't necessary as they're the defaults on recent versions
# of rpm, and shouldn't be overridden here anyway.  if you do want
# to override them you should use the %{_prefix} version.
#%define prefix		/usr
#%define sysconfdir	/etc

Name:		%name
Vendor:		GNOME
Distribution:	CVS
Summary:	Nautilus is a network user environment
Version: 	%ver
Release: 	%rel
Copyright: 	GPL
Group:		User Interface/Desktop
Source: 	%{name}-%{ver}.tar.gz
URL: 		http://nautilus.eazel.com/
BuildRoot:	%{_tmppath}/%{name}-%{ver}-root
Requires:	glib >= 1.2.9
Requires:	gtk+ >= 1.2.9
Requires:	imlib >= 1.9.8
Requires:	libxml >= 1.8.10
Requires:	gnome-libs >= 1.2.11
Requires:	GConf >= 0.12
Requires:	ORBit >= 0.5.7
Requires:	oaf >= 0.6.5
Requires:	gnome-vfs >= 1.0
Requires:	gdk-pixbuf >= 0.10.0
Requires:	bonobo >= 0.37
Requires:	popt >= 1.5
Requires:	freetype >= 2.0
Requires:	medusa >= 0.5
Requires:	esound >= 0.2.22
Requires:	libghttp >= 1.0.9
Requires:	scrollkeeper >= 0.1.4
Requires:	libpng
Requires:	control-center >= 1.3

%description
Nautilus integrates access to files, applications, media, Internet-based
resources and the Web.  Nautilus delivers a dynamic and rich user
experience.  Nautilus is an free software project developed under the
GNU General Public License and is a core component of the GNOME desktop
project.

%package devel
Summary:	Libraries and include files for developing Nautilus components
Group:		Development/Libraries
Requires:	%name = %{PACKAGE_VERSION}

%package mozilla
Summary:        Nautilus component for use with Mozilla
Group:          User Interface/Desktop
Requires:       %name = %{PACKAGE_VERSION}
Requires:	mozilla >= 0.8
Requires:	mozilla-mail >= 0.8
Requires:	mozilla-psm >= 0.8
Conflicts:	mozilla = M18
Conflicts:	mozilla = M17

%package trilobite
Summary:        Nautilus component framework for services
Group:          User Interface/Desktop
Requires:       %name = %{PACKAGE_VERSION}
Requires:	ammonite >= 0.1
Requires:	rpm >= 4.0.2
Requires:	usermode >= 1.37
Requires:       pam >= 0.72

%package extras
Summary:	Extra goodies to use with Nautilus
Group:          User Interface/Desktop
Requires:	xpdf >= 0.90
Requires:	mpg123
Requires:	sox
Requires:	gnome-user-docs >= 1.3

%package suggested
Summary:	Nautilus and a suggested set of components
Group:          User Interface/Desktop
Requires:       %name = %{PACKAGE_VERSION}
Requires:	%name-mozilla = %{PACKAGE_VERSION}
Requires:	%name-trilobite = %{PACKAGE_VERSION}
Requires:	%name-extras = %{PACKAGE_VERSION}
Requires:	mozilla-mail >= 0.8
Requires:	mozilla-psm >= 0.8

%description devel
This package provides the necessary development libraries and include
files to allow you to develop Nautilus components.

%description mozilla
This enables the use of embedded Mozilla as a Nautilus component.

%description trilobite
This is a framework library for service components in Nautilus.  It is
required by all Eazel Services, including the package installer, and
can be used to develop new services.

%description suggested
This is a meta-package that requires packages useful for running
Nautilus, and getting multimedia to work, such as eog and mpg123.

%description extras
This is a meta-package that requires useful add-ons for Nautilus.

%changelog
* Mon Mar 26 2001 Jurgen Botz <jurgen@botz.org>
- changed all %{prefix} to the more specific directory macros or %{_prefix}
- parameterized file owner %fowner, %fgroup
- use various other macros to make things cleaner

* Tue Oct 10 2000 Robin Slomkowski <rslomkow@eazel.com>
- removed obsoletes from sub packages and added mozilla and trilobite
subpackages

* Wed Apr 26 2000 Ramiro Estrugo <ramiro@eazel.com>
- created this thing

%prep
%setup

%build

# this probably shouldn't be in the released version of the sources
# MESSAGE="Eazel candidate build on "`date +%c`
#	--with-build-message="$MESSAGE" \

CFLAGS="$RPM_OPT_FLAGS -DENABLE_SCROLLKEEPER_SUPPORT"; export CFLAGS
LC_ALL=""; LINGUAS=""; LANG=""; export LC_ALL LINGUAS LANG

%configure \
	--enable-eazel-services \
	--enable-more-warnings

%make -k
%make check

%install
%setupbuildroot
%makeinstall

for FILE in "$RPM_BUILD_ROOT/bin/*"; do
	file "$FILE" | grep -q not\ stripped && strip $FILE
done

%clean
%cleanbuildroot

%post
if ! grep %{prefix}/lib /etc/ld.so.conf > /dev/null ; then
	echo "%{prefix}/lib" >> /etc/ld.so.conf
fi
/sbin/ldconfig
scrollkeeper-update

%postun -p /sbin/ldconfig
scrollkeeper-update

%files
%defattr(-, %fowner, %fgroup)
%doc AUTHORS COPYING COPYING-DOCS COPYING.LIB TRADEMARK_NOTICE ChangeLog NEWS README
%{_bindir}/nautilus-clean.sh
%{_bindir}/nautilus-verify-rpm.sh
%{_bindir}/eazel-helper
%{_bindir}/gnome-db2html2
%{_bindir}/gnome-info2html2
%{_bindir}/gnome-man2html2
%{_bindir}/hyperbola
%{_bindir}/nautilus
%{_bindir}/nautilus-adapter
%{_bindir}/nautilus-authenticate
%{_bindir}/nautilus-content-loser
%{_bindir}/nautilus-error-dialog
%{_bindir}/nautilus-hardware-view
%{_bindir}/nautilus-history-view
%{_bindir}/nautilus-image-view
# %{_bindir}/nautilus-mpg123
%{_bindir}/nautilus-music-view
%{_bindir}/nautilus-notes
%{_bindir}/nautilus-sample-content-view
%{_bindir}/nautilus-sidebar-loser
%{_bindir}/nautilus-text-view
%{_bindir}/nautilus-throbber
%{_bindir}/run-nautilus
%{_bindir}/nautilus-launcher-applet
%{_bindir}/nautilus-xml-migrate
%{_prefix}/idl/*.idl
%{_libdir}/libnautilus-adapter.so.0
%{_libdir}/libnautilus-adapter.so.0.0.0
%{_libdir}/libnautilus-extensions.so.0
%{_libdir}/libnautilus-extensions.so.0.0.0
%{_libdir}/libnautilus-tree-view.so.0
%{_libdir}/libnautilus-tree-view.so.0.0.0
%{_libdir}/libnautilus.so.0
%{_libdir}/libnautilus.so.0.0.0
%{_libdir}/libnautilus-adapter.so
%{_libdir}/libnautilus-extensions.so
%{_libdir}/libnautilus-tree-view.so
%{_libdir}/libnautilus.so

%{_libdir}/vfs/modules/*.so

%config %{_sysconfdir}/vfs/modules/*.conf
%config %{_sysconfdir}/CORBA/servers/nautilus-launcher-applet.gnorba
%{_datadir}/gnome/apps/Applications/*.desktop
%{_datadir}/gnome/ui/*.xml
%{_datadir}/nautilus/components/hyperbola/maps/*.map
%{_datadir}/nautilus/components/hyperbola/*.xml
%{_datadir}/locale/*/LC_MESSAGES/*.mo
#%{_datadir}/nautilus/*.xml
%{_datadir}/nautilus/emblems/*.png
%{_datadir}/nautilus/fonts/urw/*.dir
%{_datadir}/nautilus/fonts/urw/*.pfb
%{_datadir}/nautilus/fonts/urw/*.afm
%{_datadir}/nautilus/fonts/urw/*.pfm
%{_datadir}/nautilus/linksets/*.xml
%{_datadir}/nautilus/patterns/*.jpg
%{_datadir}/nautilus/patterns/*.png
%{_datadir}/nautilus/patterns/.*.png
%{_datadir}/nautilus/services/text/*.xml
%{_datadir}/pixmaps/*.png
%{_datadir}/pixmaps/nautilus/*.gif
%{_datadir}/pixmaps/nautilus/*.png
%{_datadir}/pixmaps/nautilus/*.svg
%{_datadir}/pixmaps/nautilus/*.xml
%{_datadir}/pixmaps/nautilus/ardmore/*.png
%{_datadir}/pixmaps/nautilus/ardmore/*.xml
%{_datadir}/pixmaps/nautilus/arlo/*.png
%{_datadir}/pixmaps/nautilus/arlo/*.xml
%{_datadir}/pixmaps/nautilus/arlo/throbber/*.png
%{_datadir}/pixmaps/nautilus/arlo/backgrounds/*.png
%{_datadir}/pixmaps/nautilus/arlo/sidebar_tab_pieces/*.png
%{_datadir}/pixmaps/nautilus/crux_eggplant/*.png
%{_datadir}/pixmaps/nautilus/crux_eggplant/*.xml
%{_datadir}/pixmaps/nautilus/crux_eggplant/backgrounds/*.png
%{_datadir}/pixmaps/nautilus/crux_eggplant/sidebar_tab_pieces/*.png
%{_datadir}/pixmaps/nautilus/gnome/*.png
%{_datadir}/pixmaps/nautilus/gnome/*.xml
%{_datadir}/pixmaps/nautilus/gnome/throbber/*.png
%{_datadir}/pixmaps/nautilus/sidebar_tab_pieces/*.png
%{_datadir}/pixmaps/nautilus/throbber/*.png
%{_datadir}/pixmaps/nautilus/gray_tab_pieces/*.png
%{_datadir}/pixmaps/nautilus/villanova/*.xml
%{_datadir}/pixmaps/nautilus/villanova/*.png
%{_datadir}/oaf/Nautilus_View_help.oaf
%{_datadir}/oaf/Nautilus_ComponentAdapterFactory_std.oaf
%{_datadir}/oaf/Nautilus_View_content-loser.oaf
%{_datadir}/oaf/Nautilus_View_hardware.oaf
%{_datadir}/oaf/Nautilus_View_history.oaf
%{_datadir}/oaf/Nautilus_View_image.oaf
%{_datadir}/oaf/Nautilus_View_music.oaf
%{_datadir}/oaf/Nautilus_View_notes.oaf
%{_datadir}/oaf/Nautilus_View_sample.oaf
%{_datadir}/oaf/Nautilus_View_sidebar-loser.oaf
%{_datadir}/oaf/Nautilus_View_text.oaf
%{_datadir}/oaf/Nautilus_View_tree.oaf
%{_datadir}/oaf/Nautilus_shell.oaf
%{_datadir}/oaf/Nautilus_Control_throbber.oaf
%{_datadir}/omf/nautilus/*.omf

%defattr (-, %fowner, %fgroup)
%{_datadir}/gnome/help

%files devel
%defattr(-, %fowner, %fgroup)
%{_libdir}/*.la
%{_libdir}/vfs/modules/*.la
%{_includedir}/libtrilobite/eazel/*/*.h
%{_includedir}/libnautilus/*.h
%{_includedir}/libtrilobite/*.h

%files mozilla
%defattr(-, %fowner, %fgroup)
%{_bindir}/nautilus-mozilla-content-view
%{_datadir}/oaf/Nautilus_View_mozilla.oaf

%files trilobite
%defattr(-, %fowner, %fgroup)
%{_bindir}/eazel-install
%{_bindir}/nautilus-service-install-view
%{_bindir}/trilobite-eazel-install-service
%{_bindir}/nautilus-summary-view
%{_bindir}/nautilus-change-password-view
#%{_bindir}/nautilus-rpm-view
%{_bindir}/eazel-gen-xml
%{_bindir}/eazel-inventory-client
%{_bindir}/trilobite-inventory-service
%{_bindir}/nautilus-inventory-view
%{_libdir}/libeazelinstall.so.0
%{_libdir}/libeazelinstall.so.0.0.0
%{_libdir}/libeazelpackagesystem.so
%{_libdir}/libeazelpackagesystem.so.0
%{_libdir}/libeazelpackagesystem.so.0.0.0
%{_libdir}/libeazelpackagesystem-rpm*.so
%{_libdir}/libeazelpackagesystem-rpm*.so.0
%{_libdir}/libeazelpackagesystem-rpm*.so.0.0.0
%{_libdir}/libtrilobite-inventory-service*so*
%{_libdir}/libtrilobite-service.so.0
%{_libdir}/libtrilobite-service.so.0.0.0
%{_libdir}/libtrilobite.so.0
%{_libdir}/libtrilobite.so.0.0.0
%{_libdir}/libeazelinstall.so
%{_libdir}/libtrilobite-service.so
%{_libdir}/libtrilobite.so

%defattr(-, %fowner, %fgroup)
%config %{_sysconfdir}/pam.d/eazel-helper
%config %{_sysconfdir}/security/console.apps/eazel-helper
%{_datadir}/oaf/Nautilus_View_install.oaf
%{_datadir}/oaf/Trilobite_Service_install.oaf
%{_datadir}/oaf/Nautilus_View_change-password.oaf
%{_datadir}/oaf/Nautilus_View_services-summary.oaf
#%{_datadir}/oaf/Nautilus_View_rpm.oaf
%{_datadir}/oaf/Trilobite_Service_inventory.oaf
%{_datadir}/oaf/Nautilus_View_inventory.oaf

%files extras
%defattr(-, %fowner, %fgroup)
%{_datadir}/nautilus/nautilus-extras.placeholder

%files suggested
%defattr(-, %fowner, %fgroup)
%{_datadir}/nautilus/nautilus-suggested.placeholder
