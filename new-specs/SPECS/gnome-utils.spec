# Note that this is NOT a relocatable package
%define  ver     1.4.0
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:        GNOME utility programs
Name: 		gnome-utils
Version: 	%ver
Release: 	%rel
Copyright: 	LGPL
Group: 		Applications/System
Source: 	ftp://ftp.gnome.org/pub/GNOME/stable/sources/gnome-utils-%{ver}.tar.gz
BuildRoot: 	%{_tmppath}/gnome-utils-%{PACKAGE_VERSION}-root
URL: 		http://www.gnome.org
Requires: 	gnome-libs >= 1.0.59

%description
GNOME (GNU Network Object Model Environment) is a user-friendly set of
applications and desktop tools to be used in conjunction with a window
manager for the X Window System.  GNOME is similar in purpose and scope
to CDE and KDE, but GNOME is based completely on free software.

This package will install some GNOME utilities, such as the
calculator, search tool, and system information overview tool.

%prep
%setup -q

%build

%configure
%make

%install
%setupbuildroot
%makeinstall


# ###########################################################################
#  A collection of scriptlet functions to ease the maintence of rpm spec
#  files. They are designed to help build dynamic %files sections.
#
# The only problem with these damn things is that it makes it very
# dificult to put the right macros in the ./configure/make install
# steps to ensure that everything goes where people want it to go.
# gleblanc@cu-portland.edu

function ProcessLang() {
  # This will handle the locale stuff inserting the language specific 
  # tags before each bit. This will use the included find-lang.sh included
  # in rpm 3.0.xx +
  #
  # Usage: ProcessLang <output file> <attr>
  #
  # output file -- Where we should append the output.
  # attr        -- what should be inserted in the %defattr () line before 
  #                this section.
  if [ -f /usr/lib/rpm/find-lang.sh ] ; then
    /usr/lib/rpm/find-lang.sh $RPM_BUILD_ROOT %name
    if [ -f %{name}.lang ] ; then
      if [ ! -z "$2" ] ; then
        sed "s:(644, root, root, 755):$2:g" %{name}.lang >tmp.lang && mv tmp.lang %{name}.lang
      fi
      if [ ! -z "$1" ] ; then
        cat %{name}.lang >> "$1"
        rm -f %{name}.lang
      fi
    fi
  fi
}

function ProcessBin() {
  # Gather up all the executable files. Stripping if requested.
  # This will not recurse.
  # 
  # Usage: ProcessBin <output file> <attr> <strip>
  #
  if [ -d $RPM_BUILD_ROOT%{_bindir} ] ; then
    echo "%defattr (0555, %fowner, %fgroup)" >>%{name}.files
    find $RPM_BUILD_ROOT%{_bindir} -type f -print | sed "s:^$RPM_BUILD_ROOT::g" >>%{name}.files
  fi
}

function ProcessLib() {
  # Gather up any libraries.
  # Usage: ProcessLib <output file> <dir> <type>
  # Type is either 'runtime' or 'devel'
  if [ -d $2 ] ; then
    echo "%defattr (0555, %fowner, %fgroup)" >>$3
    case "$3" in
      runtime)
       # Grab runtime libraries
       find $2 -name "*.so.*" -print | sed "s:^$RPM_BUILD_ROOT::g" >>$1
       ;;
      devel)
       find $2 -name "*.so" -print | sed "s:^$RPM_BUILD_ROOT::g" >>$1
       find $2 -name "*.la" -print | sed "s:^$RPM_BUILD_ROOT::g" >>$1
       find $2 -name "*.a" -print | sed "s:^$RPM_BUILD_ROOT::g" >>$1
       find $2 -name "*.sh" -print | sed "s:^$RPM_BUILD_ROOT::g" >>$1
       ;;
    esac
   fi
}

function ProcessDir() {
  # Build a list of files in the specified dir sticking
  # a %defattr line as specified in front of the mess. This is intended
  # for normal dirs. Use ProcessLib for library dirs
  # for include dirs. Appending to <output file>.
  # This will recurse.
  #
  # Usage: ProcessDir <dir> <output file> <attr>
  #
  if [ -d $1 ] ; then
   if [ ! -z "$3" ] ; then
     echo "%defattr ($3)" >>$2
   fi
   echo "*** Processing $1"
   find $1 -type f -print | sed "s:^$RPM_BUILD_ROOT::g" >>$2
  fi
}
###
## Custom bit
###
function BuildFiles() {
   ProcessBin
   ProcessLang %{name}.files
   for i in `find $RPM_BUILD_ROOT%{_datadir} -maxdepth 1 -type d -print | \
    sed "s:^$RPM_BUILD_ROOT%{_prefix}/share::g"` ; do
    echo $i
    case $i in
     /locale)
      echo "Skipping $i"
      ;;
    *)
      ProcessDir $RPM_BUILD_ROOT%{_datadir}$i %{name}.files "0444, root, root, 0555"
      ;;
    esac
   done
   ProcessDir $RPM_BUILD_ROOT/etc %{name}.files "0444, root, root, 0555"
}

############################################################################
# Now lets build up a %files section.
if [ -f %{name}.files ] ; then
  rm -f %{name}.files
fi

BuildFiles


%clean
%cleanbuildroot

%files -f %{name}.files
%defattr(-, %fowner, %fgroup)
%doc AUTHORS COPYING ChangeLog NEWS README

%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- macro fixes and removal of hard-coded paths.

* Wed Sep 08 1999 Elliot Lee <sopwith@redhat.com>
- Updates from RHL package...

* Wed Sep 23 1998 Michael Fulbright <msf@redhat.com>

- Upgraded to 0.30

* Mon Apr  6 1998 Marc Ewing <marc@redhat.com>

- Integrate into gnome-utils CVS source tree
