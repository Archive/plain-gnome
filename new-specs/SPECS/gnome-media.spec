# Note that this is NOT a relocatable package
%define  ver     1.2.0
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:       GNOME media programs
Name:          gnome-media
Version:       %ver
Release:       %rel
Copyright:     LGPL
Group:         X11/Libraries
Source:        gnome-media-%{ver}.tar.gz
BuildRoot:     %{_tmppath}/gnome-media-%{PACKAGE_VERSION}-root
Obsoletes:     gnome
URL:           http://www.gnome.org
Requires:      gnome-libs >= 0.99.8
Summary(es):   Programas multimedia de GNOME
Summary(fr):   Programmes multim�dia de GNOME

%description
GNOME media programs.

GNOME is the GNU Network Object Model Environment.  That's a fancy
name but really GNOME is a nice GUI desktop environment.  It makes
using your computer easy, powerful, and easy to configure.

%description -l es
Programas multimedia GNOME.

GNOME (GNU Network Object Model Environment) es un entorno gr�fico
orientado escritorio. Con �l el uso de su computadora es m�s f�cil,
agradable y eficaz.

Este paquete contiene varios juegos para el entorno Gnome.

%description -l fr
Programmes multim�dia GNOME.

GNOME (GNU Network Object Model Environment) est un environnement graphique
de type bureau. Il rends l'utilisation de votre ordinateur plus facile,
agr�able et eficace, et est facile � configurer.

%prep
%setup

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS COPYING ChangeLog NEWS README

%{_bindir}/*
%{_datadir}/locale/*/*/*
%config %{_datadir}/gnome/cddb-submit-methods
%{_datadir}/gnome/apps/*
%{_datadir}/pixmaps/*
%{_datadir}/mime-info/*


%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- updated macros and fixed hard-coded paths.  

* Sat Nov 21 1998 Pablo Saratxaga <srtxg@chanae.alphanet.ch>
- added spanish and french translations for rpm

* Wed Sep 23 1998 Michael Fulbright <msf@redhat.com>
- Updated to 0.30 release

* Mon Mar 16 1998 Marc Ewing <marc@redhat.com>
- Integrate into gnome-media CVS source tree
