%define name	 GConf
%define ver      1.0.0
%define RELEASE  0_plain_0
%define rel      %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     Gnome Config System
Name:        %name
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       System Environment/Base
Source:      GConf-%{ver}.tar.gz
BuildRoot:   %{_tmppath}/gconf
URL:         http://www.gnome.org
Prereq:      /sbin/install-info
Requires:    glib >= 1.2.0
Requires:    oaf >= 0.3.0
Requires:    gtk+ >= 1.2.0
Requires:    ORBit >= 0.5.0
Requires:    libxml >= 1.8.0

%description
GConf is the GNOME Configuration database system.

GNOME is the GNU Network Object Model Environment.  That's a fancy
name but really GNOME is a nice GUI desktop environment.  It makes
using your computer easy, powerful, and easy to configure.

%package devel
Summary:    Gnome Config System development package
Group:      Development/Libraries
Requires:   %{name} = %{ver}
Requires:   ORBit-devel
Requires:   glib-devel
Requires:   oaf-devel
Requires:   gtk+-devel
PreReq:     /sbin/install-info

%description devel
GConf development package. Contains files needed for doing
development using GConf.

%prep
%setup

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%post   -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS COPYING ChangeLog NEWS README
%config %{_sysconfdir}/gconf/1/*.example
%config %{_sysconfdir}/gconf/schemas/*.schemas
%{_bindir}/gconf-config
%{_bindir}/gconf-config-1
%{_bindir}/gconfd-1
%{_bindir}/gconftool
%{_bindir}/gconftool-1
%{_libdir}/*.0
%{_libdir}/*.1
%{_libdir}/*.so
%{_libdir}/GConf/1/*.so
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/oaf/*.oafinfo


## /etc/gconf/schemas/desktop.schemas is notably missing;
## it will be shared between versions of GConf, preventing
## simulataneous installation, so maybe should be in 
## a different (minuscule) package.

%files devel
%defattr(-, %fowner, %fgroup)
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/GConf/1/*.a
%{_libdir}/GConf/1/*.la
%{_includedir}/gconf/1/gconf/*.h
%{_datadir}/aclocal/*.m4


%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixed the spec file.  :-)  Removed some ugly fixed paths, and
  used macros properly  (why would I want to install this to 
  HP's home directory?) 

* Sun Jun 11 2000  Eskil Heyn Olsen <deity@eazel.com>
- Created the .spec file
