# Note that this is NOT a relocatable package
# $Id: medusa.spec,v 1.1 2001/04/07 23:53:30 jbotz Exp $

%define name	 medusa
%define ver      0.5.0
%define RELEASE  0_plain_0 
%define rel      %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary: Medusa, the search and indexing package for use with Eazel's Nautilus.
Name: %name
Version: %ver
Release: %rel
Copyright: LGPL
Group: System Environment/Base
Source: ftp://ftp.gnome.org/pub/GNOME/unstable/sources/medusa/medusa-%{ver}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{ver}-root
URL: http://www.gnome.org
Prereq: /sbin/install-info
Requires: glib >= 1.2.0
Requires: gnome-vfs >= 0.1

%description
Medusa, the GNOME search/indexing package.

%package devel
Summary:        Libraries and include files for developing nautilus components
Group:          Development/Libraries
Requires:       %name = %{PACKAGE_VERSION}
Obsoletes:      %{name}-devel

%description devel
This package provides the necessary development libraries and include 
files to allow you to develop medusa components.

%prep
%setup

%build
%configure

%make -k check

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%post
/sbin/ldconfig

INITDIR=""
for i in "/etc" "/etc/rc.d"
do
	if test -d "$i/init.d" && test -d "$i/rc0.d" && test -d "$i/rc2.d"
	then
		INITDIR="$i"
	fi
done

case "$INITDIR" in
	'')
		echo "Can't find init script directory" >&2
		exit 1
	;;
esac

rm -f "$INITDIR/rc2.d/S99medusa"
rm -f "$INITDIR/rc0.d/K00medusa"

cp "%{_prefix}/tmp/medusa-init" "$INITDIR/init.d/medusa"
ln -s ../init.d/medusa "$INITDIR/rc2.d/S99medusa"
ln -s ../init.d/medusa "$INITDIR/rc0.d/K00medusa"

%postun
/sbin/ldconfig

INITDIR=""
for i in "/etc" "/etc/rc.d"
do
	if test -d "$i/init.d" && test -d "$i/rc0.d" && test -d "$i/rc2.d"
	then
		INITDIR="$i"
	fi
done

case "$INITDIR" in
	'')
		echo "Can't find init script directory" >&2
		exit 1
	;;
esac

rm -f "$INITDIR/init.d/medusa"
rm -f "$INITDIR/rc2.d/S99medusa"
rm -f "$INITDIR/rc0.d/K00medusa"

%files
%defattr (0755, %fowner, %fgroup)
%doc AUTHORS COPYING ChangeLog NEWS README
%config %{_sysconfdir}/medusa
%config %{_sysconfdir}/cron.daily/medusa.cron
%config %{_prefix}/tmp/medusa-init
%config %{_sysconfdir}/profile.d/medusa-idled.sh
%config %{_sysconfdir}/profile.d/medusa-idled.csh
%defattr(-, bin, bin)
%config %{_sysconfdir}/vfs/modules/*.conf
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_libdir}/lib*.so
%{_libdir}/vfs/modules/*.so
%{_sbindir}/*
%{_localstatedir}/medusa
%{_mandir}/*/*


%files devel
%defattr(-, %fowner, %fgroup)
%{_includedir}/libmedusa
%{_libdir}/*.la
%{_libdir}/vfs/modules/*.la


%changelog
* Wed Feb 21 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- updates and fixed for hard-coded paths, and poor macro use.

* Sun Jun 11 2000  Eskil Heyn Olsen <deity@eazel.com>
- Created the .spec file
