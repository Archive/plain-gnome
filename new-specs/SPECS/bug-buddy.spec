%define ver	        2.0
%define RELEASE	        0_plain_0
%define rel	        %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

%define _localstatedir  /var/lib


Summary:      Utility to ease the reporting of bugs within the GNOME Desktop Environment.
Name:         bug-buddy
Version:      %ver
Release:      %rel
Copyright:    GPL
Group:        Applications/System
Source:       bug-buddy-%{ver}.tar.gz
URL:          http://www.andrew.cmu.edu/~jberkman/bug-buddy/
BuildRoot:    %{_tmppath}/bug-buddy-root

Requires: gnome-libs >= 1.0.60
Requires: libglade >= 0.5

%description
The goal of bug-buddy is to make reporting bugs very simple
and easy for users, while making the reports themselves more
useful and informative for developers. 

%prep
%setup

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%files
%defattr(-, %fowner, %fgroup)

%doc ABOUT-NLS AUTHORS ChangeLog COPYING INSTALL NEWS README TODO
%{_bindir}/*
%{_datadir}/pixmaps/*
%{_datadir}/gnome/*
%{_datadir}/mime-info/*
%{_datadir}/bug-buddy
%{_datadir}/locale/*


%changelog
* Tue Feb 20 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- macro fixes, remove hard-coded paths

* Wed Feb 23 2000  Jacob Berkman  <jacob@helixcode.com>
- don't say we own the pixmaps/ dir

* Wed Nov 10 1999  Jacob Berkman  <jberkman@andrew.cmu.edu>
- fixed up for insertion into the CVS

* Fri Nov 05 1999  Ned Rhudy  <nrhudy@pabus.com>
- the first incarnation of the spec file...watch it not work
