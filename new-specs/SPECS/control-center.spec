%define  ver     1.4.0.1
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     The GNOME control center.
Name:        control-center
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       System Environment/Libraries
Source:      control-center-%{version}.tar.gz
BuildRoot:   %{_tmppath}/control-center-%{version}-root
Obsoletes:   gnome
URL:         http://www.gnome.org
Requires:    xscreensaver >= 3.00
Requires:    gnome-libs >= 1.0.0
Requires:    ORBit >= 0.4.0

%description
Control-center is a configuration tool for easily
setting up your GNOME environment.

GNOME is the GNU Network Object Model Environment. That's
a fancy name, but really GNOME is a nice GUI desktop 
environment. 

It's a powerful, easy to configure environment which
helps to make your computer easy to use.

%package devel
Summary: GNOME control-center development files.
Group: System Environment/Libraries
Requires: control-center

%description devel
If you're interested in developing panels for the GNOME
control center, you'll want to install this package.

Control-center-devel helps you create the 'capplets'
which are used in the control center.


%prep
%setup

%build
%configure
%make

%install
%setupbuildroot
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}
%makeinstall

%clean
%cleanbuildroot

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_sysconfdir}/CORBA/servers/*
%{_datadir}/control-center
%{_datadir}/pixmaps/*
%{_datadir}/locale/*/*/*
%{_datadir}/gnome/apps/Settings/*
%{_datadir}/gnome/wm-properties/*
%{_datadir}/gnome/help/control-center

%files devel
%defattr(-, %fowner, %fgroup)

%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/lib*.la
%{_libdir}/*Conf.sh
%{_datadir}/idl
%{_includedir}/*



%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- updated, removed hard-coded paths, added proper use of RPM macros.

* Sat Feb 27 1999 Gregory McLean <gregm@comstar.net>
- version 1.0.0
- updated the requirements to match the current released versions of packages.
- Added an alpha specfic rule as libtool don't understand _all_ alpha models.

* Mon Feb 15 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.8.1
- added etc/CORBA/servers/* to file list

* Fri Feb 12 1999 Michael Fulbright <drmike@redhat.com>
- update to 0.99.8
- added /usr/lib/cappletConf.sh

* Mon Feb 08 1999 The Rasterman <raster@redhat.com>
- update to 0.99.5.1

* Wed Feb 03 1999 Michael Fulbright <drmike@redhat.com>
- update to 0.99.5

* Mon Jan 20 1999 Michael Fulbright <drmike@redhat.com>
- update to 0.99.3.1

* Mon Jan 18 1999 Michael Fulbright <drmike@redhat.com>
- update to 0.99.3
- seems like patch for non-standard xscreensaver placement was already in
  prestine sources(?)

* Wed Jan 06 1999 Jonathan Blandford <jrb@redhat.com>
- updated to 0.99.1
- temporary hack patch to get path to work to non-standard placement
  of xscreensaver binaries in RH 5.2

* Wed Dec 16 1998 Jonathan Blandford <jrb@redhat.com>
- Created for the new control-center branch
