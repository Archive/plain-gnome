%define ver	1.2.13
%define RELEASE	0_plain_0
%define rel	%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

%define	preferdb	--enable-prefer-db1

Name:		gnome-libs
Version: 	%ver
Release:	%rel
Summary:	Main GNOME libraries
Copyright:	LGPL
Group:		System Environment/Libraries
Source: 	%{name}-%{version}.tar.gz
URL:		http://www.gnome.org/
BuildRoot:	%{_tmppath}/%{name}-%{ver}-root
Requires:	gtk+ >= 1.2.5
Requires:	ORBit
Requires:	imlib
Requires:	esound

%description
GNOME (GNU Network Object Model Environment) is a user-friendly set of
GUI applications and desktop tools to be used in conjunction with a
window manager for the X Window System.  The gnome-libs package
includes libraries that are needed to run GNOME.

%package devel
Summary:	Libraries and headers for GNOME application development
Group:		Development/Libraries
Requires:	%name = %{version}
Requires:	gtk+-devel
Requires:	ORBit-devel
Requires:	imlib-devel
Requires:	esound-devel
#Requires:	db1-devel

%description devel
GNOME (GNU Network Object Model Environment) is a user-friendly set of
GUI applications and desktop tools to be used in conjunction with a
window manager for the X Window System. The gnome-libs-devel package
includes the libraries and include files that you will need to develop
GNOME applications.

You should install the gnome-libs-devel package if you would like to
develop GNOME applications.  You don''t need to install gnome-libs-devel
if you just want to use the GNOME desktop environment.

%prep
%setup -q

%build

%configure %preferdb
%make

%install
%setupbuildroot
%makeinstall

mv $RPM_BUILD_ROOT%{_datadir}/doc/gnome-doc $RPM_BUILD_ROOT%{_bindir}
chmod a+x $RPM_BUILD_ROOT%{_bindir}/gnome-doc
mv $RPM_BUILD_ROOT%{_datadir}/doc/mkstub $RPM_BUILD_ROOT%{_bindir}
chmod a+x $RPM_BUILD_ROOT%{_bindir}/mkstub
mkdir -p $RPM_BUILD_ROOT%{_datadir}/emacs/site-lisp
mv $RPM_BUILD_ROOT%{_datadir}/doc/gnome-doc.el $RPM_BUILD_ROOT%{_datadir}/emacs/site-lisp

%clean
%cleanbuildroot

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS COPYING ChangeLog NEWS README
# this is a hack, but at least we get everything this way
%doc devel-docs

%{_libdir}/lib*.so.*
%{_bindir}/dns-helper
%{_bindir}/gconfigger
%{_bindir}/gnome-bug
%{_bindir}/gnome-dump-metadata
%{_bindir}/gnome-gen-mimedb
%{_bindir}/gnome-moz-remote
%{_bindir}/gnome-name-service
%{_bindir}/gnome_segv
%{_bindir}/goad-browser
%{_bindir}/loadshlib
%{_bindir}/new-object
%attr(2755, root, utmp) %{_sbindir}/gnome-pty-helper
%{_datadir}/locale/*/*/*
%{_datadir}/idl/*
%{_datadir}/pixmaps/*
%{_datadir}/mime-info/gnome.mime
%{_datadir}/type-convert/type.convert
%{_mandir}/*/*
%config %{_sysconfdir}/*

%files devel
%defattr(-, %fowner, %fgroup)

%{_bindir}/gnome-config
%{_bindir}/libart-config
%{_bindir}/gnome-doc
%{_bindir}/mkstub
%{_libdir}/lib*.so
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.sh
%{_libdir}/gnome-libs
%{_includedir}/*
%{_datadir}/aclocal/*
%{_datadir}/gnome/html
%{_datadir}/emacs/site-lisp/gnome-doc.el

%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixed macros and hard-coded paths.

* Wed Jan 31 2001 Akira TAGOH <tagoh@redhat.com>
- Fixed show multibyte character in gnome_canvas_new_aa ()

* Sun Jan 28 2001 Akira TAGOH <tagoh@redhat.com>
- Fixed show UTF-8 encoding KDE menu.

* Wed Jan 24 2001 Matt Wilson <msw@redhat.com>
- Japanese ideographs now show up in iswalnum, don't include it in the
  ideograph check (Patch26: gtk-xmhtml-wordwrap-alnum.patch)

* Wed Dec 27 2000 Matt Wilson <msw@redhat.com>
- enable japanese
- removed gnome-libs-1.2.4-druid-fontset.patch, in mainline

* Tue Dec 19 2000 Matt Wilson <msw@redhat.com>
- disabled i18n (asian) patches, but integrate patches into main package
- added iconlist im patch from CLE, currently disabled.
- 1.2.8
- removed gnome-libs-1.2.4-setbgfix.patch, in mainline
- removed gnome-libs-1.2.4-grabfix.patch, in mainline
- removed gnome-libs-1.2.4-doubleclicks.patch, in mainline
- removed gnome-libs-1.2.4-motionnotify.patch, in mainline
- removed gnome-libs-1.2.4-nosound.patch, in mainline
- removed alpha cflags hack

* Thu Sep  7 2000 Matt Wilson <msw@redhat.com>
- added patch for gtk-XmHTML wrapping
- added patch to ensure proper kanji display in about boxes (is this still needed?)
- added patch to tweak the fonts used for XmHTML to be readable in Japanese

* Tue Aug 29 2000 Yukihiro Nakai <ynakai@redhat.com>
- Add Japanese patches

* Wed Aug 23 2000 Elliot Lee <sopwith@redhat.com>
- Fix the mismatch of default settings between control-center and
gnome-libs. I am the true survivor!

* Sat Aug 19 2000 Havoc Pennington <hp@redhat.com>
- Always use SCROLL_NEVER mode as if transparency was on, 
  even if transparency is not on. This fixes 14744 but makes
  scrolling slow. Best we can do, sigh.

* Sat Aug 19 2000 Havoc Pennington <hp@redhat.com>
- Call gdk_window_get_pointer on motion notify, fixes bug
  16407, though I don't quite understand why it does

* Fri Aug 11 2000 Jonathan Blandford <jrb@redhat.com>
- Up Epoch and release

* Wed Aug 09 2000 Havoc Pennington <hp@redhat.com>
- Filter out double/triple clicks when doing scroll or paste

* Wed Aug 09 2000 Havoc Pennington <hp@redhat.com>
- fix for grabbing the pointer while selecting

* Sat Aug 05 2000 Havoc Pennington <hp@redhat.com>
- fix for failure to change background color post-realization

* Fri Aug 04 2000 Owen Taylor <otaylor@redhat.com>
- Add a Requires db1-devel to the devel package. (Bug #15394)

* Thu Aug 03 2000 Owen Taylor <otaylor@redhat.com>
- Rebuild to see if we can fix funny gnome-config junk
  problem. 

* Tue Jul 19 2000 Havoc Pennington <hp@redhat.com>
- Rebuild; were getting weird segfaults with tearoff dock items,
  rebuilding with debugging made it go away, hoping it was 
  a compiler bug.

* Wed Jul 12 2000 Havoc Pennington <hp@redhat.com>
- 1.2.4
- remove IDL fix patch, seems to have gone upstream

* Mon Jun 19 2000 Havoc Pennington <hp@redhat.com>
- Move elisp files and executables out of /usr/share/doc
- Add HTML docs to file list
- remove broken %%doc mess

* Thu Jun 15 2000 Jonathan Blandford <jrb@redhat.com>
- added bug fix to fix idl miscompilation.

* Fri Jun  9 2000 Matt Wilson <msw@redhat.com>
- 1.2.1
- rebuilt with gcc that has fixed C ABI

* Tue May 30 2000 Matt Wilson <msw@redhat.com>
- prefer db1

* Fri May 19 2000 Owen Taylor <otaylor@redhat.com>
- Upgrade to gnome-libs 1.0.62

* Mon Feb 21 2000 Preston Brown <pbrown@redhat.com>
- keyboard mapping follows debian policy, xterm

* Tue Feb 15 2000 Owen Taylor <otaylor@redhat.com>
- Add checks to make sure gnome-terminal survives if the
  bg pixmap vanishes.

* Fri Feb 11 2000 Owen Taylor <otaylor@redhat.com>
- Install gtkrc files in /etc/gnome

* Fri Feb 11 2000 Owen Taylor <otaylor@redhat.com>
- Remove broken alpha timeval fix, fix bug in configure.in
  that was misdetecting ut_tv field of utmpx

* Thu Feb 10 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix up escape sequences in zvt (Bug #9143)

* Fri Feb 04 2000 Elliot Lee <sopwith@redhat.com>
- Accept true/yes/non-zero as TRUE for gnome-config boolean values.

* Wed Feb 03 2000 Havoc Pennington <hp@redhat.com>
- set ECHOK for terminal widget, bug 8823

* Wed Feb 02 2000 Havoc Pennington <hp@redhat.com>
- include man pages in the file list, also bug 8017

* Wed Feb 02 2000 Havoc Pennington <hp@redhat.com>
- Fix an alpha warning bugzilla #8017

* Tue Feb 01 2000 Elliot Lee <sopwith@redhat.com>
- Update to 1.0.55

* Tue Aug 31 1999 Elliot Lee <sopwith@redhat.com>
- Update to 1.0.15

* Mon Aug 30 1999 Elliot Lee <sopwith@redhat.com>
- Merge in various minor things from RHL

* Mon Jun 14 1999 Gregory McLean <gregm@comstar.net>

- Added the -q option to the setup stage, quiet please!

* Tue Mar 2  1999 Gregory McLean <gregm@comstar.net>

- Added some hackage in for the brain dead libtool on the alpha
- Cleaned up the spec file abit to be more consistant.

* Wed Feb 17 1999 Elliot Lee <sopwith@redhat.com>

- Add debugging disabling flags to $CFLAGS

* Fri Nov 20 1998 Pablo Saratxaga <srtxg@chanae.alphanet.ch>

- use --localstatedir=/var/lib in config state (score files for games
  for exemple will go there).
- added several more files to %files section, in particular language
  files and corba IDLs

* Wed Sep 23 1998 Michael Fulbright <msf@redhat.com>

- Updated to version 0.30

* Mon Apr 13 1998 Marc Ewing <marc@redhat.com>
- Added %{prefix}/lib/gnome-libs

* Fri Mar 13 1998 Marc Ewing <marc@redhat.com>

- Integrate into gnome-libs source tree
