# Note that this is NOT a relocatable package
%define  name    gnome-vfs
%define  ver     1.0
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:        The GNOME virtual file-system libraries
Name:           %name
Version:        %ver
Release:        %rel
Vendor:		CVS
Distribution:	GNOME
Copyright:      LGPL
Group:          System Environment/Libraries
Source:         %name-%{ver}.tar.gz

URL:            http://www.gnome.org/
BuildRoot:      %{_tmppath}/%{name}-root
Requires:       glib >= 1.2.4
Requires:       GConf >= 0.9
Requires:       oaf >= 0.3.0

%description
GNOME VFS is the GNOME virtual file system. It is the foundation of the
Nautilus file manager. It provides a modular architecture and ships with
several modules that implement support for file systems, http, ftp and others.
It provides a URI-based API, a backend supporting asynchronous file operations,
a MIME type manipulation library and other features.

%package devel
Summary:    Libraries and include files for developing GNOME VFS applications.
Group:      Development/Libraries
Requires:   %name = %{version}
Requires:   GConf-devel
Requires:   oaf-devel

%description devel
This package provides the necessary development libraries for writing
GNOME VFS modules and applications that use the GNOME VFS APIs.

%prep
%setup -q

%build
%configure --enable-more-warnings
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%post
if ! grep %{_libdir} /etc/ld.so.conf > /dev/null ; then
  echo "%{_prefix}/lib" >> /etc/ld.so.conf
fi
/sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS COPYING ChangeLog NEWS README
%config %{_sysconfdir}/gnome-vfs-mime-magic
%config %{_sysconfdir}/vfs/modules/*.conf
%dir %{_datadir}/application-registry
%{_bindir}/gnome-vfs-config
%{_bindir}/gnome-vfs-slave
#%{_bindir}/nautilus-mime-type-capplet
%{_libdir}/*.0
%{_libdir}/*.sh
%{_libdir}/*.so
%{_libdir}/vfs/extfs/*
%{_libdir}/vfs/modules/*.0
%{_libdir}/vfs/modules/*.so
%{_mandir}/man5/*.5*
%{_datadir}/application-registry/gnome-vfs.applications
#%{_datadir}/control-center/*.desktop
#%{_datadir}/gnome/apps/Settings/*.desktop
%{_datadir}/gnome/html/*.txt
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/mime-info/*.keys
%{_datadir}/mime-info/*.mime

%files devel
%defattr(-, %fowner, %fgroup)
%{_includedir}/libgnomevfs/*.h
%{_libdir}/vfs/include/*.h
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/vfs/modules/*.a
%{_libdir}/vfs/modules/*.la


%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fix paths and macros

* Tue Feb 22 2000 Ross Golder <rossigee@bigfoot.com>
- Integrate into source tree
