%define  ver     0.1
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Name:            panelmm
Summary:         C++ bindings for making panel applets
Version:         %ver
Release:         %rel
License:         GPL/LGPL
Source:          panelmm-%{version}.tar.gz
#Patch0:          panelmm-configure-fix.patch
Group:           System Environment/Libraries
URL:             http://www.gnome.org/
BuildRoot:       %{_tmppath}/%{name}-%{version}root
BuildRequires:   gnome-libs-devel, ORBit-devel, gtk+-devel >= 1.2.0
BuildRequires:   gtkmm-devel >= 1.1.5, gnome-core-devel

%description
Panelmm is some stuff to let people write panel applet applets in C++.

%package devel
Summary:    panelmm development libraries
Group:      Development/Libraries
Requires:   %{name} = %{version}

%description devel
This package contains the headers and libraries that programmers will
need to develop programs using panelmm.

%prep
%setupbuildroot

%setup
#%patch -p1

%build
%configure

%install
%makeinstall

%clean
%cleanbuildroot

%files
%defattr(-,%fowner,%fgroup)
%{_libdir}/*so*

%files devel
%defattr(-,%fowner,%fgroup)
%{_libdir}/lib*.a
%{_libdir}/lib*.la
%{_libdir}/*.sh
%{_includedir}/*.h
%{_includedir}/panel--

%changelog
* Sun Mar 04 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- Created panelmm spec file

