# Note that this is NOT a relocatable package
%define  ver     0.28
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:        Gnome Print - Printing libraries for GNOME.
Name: 		gnome-print
Version: 	%ver
Release: 	%rel
Copyright: 	LGPL
Group: 		System Environment/Base
Source:         gnome-print-%{ver}.tar.gz
BuildRoot: 	%{_tmppath}/%{name}-%{ver}-root
Requires: 	gnome-libs >= 1.0
Requires:       urw-fonts
Requires:       ghostscript-fonts >= 4.03
Prereq:	        /sbin/ldconfig libxml

%description
GNOME (GNU Network Object Model Environment) is a user-friendly set of
applications and desktop tools to be used in conjunction with a window
manager for the X Window System.  GNOME is similar in purpose and scope
to CDE and KDE, but GNOME is based completely on free software.
The gnome-print package contains libraries and fonts that are needed by
GNOME applications wanting to print.

You should install the gnome-print package if you intend on using any of
the GNOME applications that can print. If you would like to develop GNOME
applications that can print you will also need to install the gnome-print
devel package.

%package devel
Summary:  Libraries and include files for developing GNOME applications.
Group: 	  Development/Libraries

%description devel
GNOME (GNU Network Object Model Environment) is a user-friendly set of
applications and desktop tools to be used in conjunction with a window
manager for the X Window System.  GNOME is similar in purpose and scope
to CDE and KDE, but GNOME is based completely on free software.
The gnome-print-devel package includes the libraries and include files that
you will need when developing applications that use the GNOME printing 
facilities.

You should install the gnome-print-devel package if you would like to 
develop GNOME applications that will use the GNOME printing facilities.
You don't need to install the gnome-print-devel package if you just want 
to use the GNOME desktop enviornment.

%prep
%setup -q

%build

%configure
%make

%install
%setupbuildroot
%makeinstall

install -m 0755 run-gnome-font-install $RPM_BUILD_ROOT%{_datadir}/gnome-print/%{ver}
( cd fonts; install -m 0644 *.font $RPM_BUILD_ROOT%{_datadir}/fonts )

%clean
%cleanbuildroot

%post
if ! grep %{_libdir} /etc/ld.so.conf > /dev/null ; then
  echo "%{_libdir}" >> /etc/ld.so.conf
fi
/sbin/ldconfig
perl %{_datadir}/gnome-print/%{ver}/run-gnome-font-install \
        %{_bindir}/gnome-font-install \
        %{_datadir} \
        %{_datadir}

%postun 
/sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_datadir}/fonts/afms/adobe/*
%{_datadir}/fonts/*.font
%config %{_datadir}/gnome-print/%{ver}

%files devel
%defattr(-, %fowner, %fgroup)
%{_libdir}/lib*.so
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.sh
%{_includedir}/*/*


%changelog
* Sat Apr 07 2001 Jurgen Botz <jurgen@botz.org>
- moved run-gnome-fonts-install to %{_datadir}/gnome-print/%{ver}
  and run that in the post install script
- many other changes

* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- remove hard-coded paths, and fixed up macro use

* Sun Aug 01 1999 Gregory McLean <gregm@comstar.net>
- Undo my draconian uninstall stuff.

* Tue Jul 20 1999 Gregory McLean <gregm@comstar.net>
- Stab at cleaning up properly when we uninstall.

* Fri Jul 16 1999 Herbert Valerio Riedel <hvr@hvrlab.dhs.org>
- fixed typo in spec

* Wed Jul 14 1999 Gregory McLean <gregm@comstar.net>
- Added fonts to the spec.

* Mon Jul 05 1999 Gregory McLean <gregm@comstar.net>
- Fleshed out the descriptions.
