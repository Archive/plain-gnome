# Note that this is NOT a relocatable package
%define  ver     1.0.13
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     GNOME System Monitor
Name:        gtop
Version:     %ver
Release:     %rel
Copyright:   GPL
Group:       Applications/System
Source:      ftp://ftp.gnome.org/pub/GNOME/sources/gtop/gtop-%{ver}.tar.gz
BuildRoot:   %{_tmppath}/gtop-root
Obsoletes:   gnome
URL:         http://www.gnome.org

%description
GNOME System Monitor.

GNOME is the GNU Network Object Model Environment.  That's a fancy
name but really GNOME is a nice GUI desktop environment.  It makes
using your computer easy, powerful, and easy to configure.

%prep
%setup

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS BUG-REPORTING COPYING NEWS README
%{_bindir}/*
%{_datadir}/gnome/apps/System/gtop.desktop
#%{_datadir}/gnome/help/gtop/C/*.html
#%{_datadir}/gnome/help/gtop/C/*.dat
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/pixmaps/gnome-gtop.png


%changelog
* Wed Feb 21 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fix macros and hard-coded paths.

* Sun Aug 23 1998 Martin Baulig <martin@home-of-linux.org>
- Make GTop its own top-level module.
