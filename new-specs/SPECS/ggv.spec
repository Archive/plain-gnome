%define  ver     1.0
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     GNOME PostScript viewer
Name: 	     ggv
Version:     %ver
Release:     %rel
Copyright:   GPL
Group: 	     X11/Utilities
Source:	     %{name}-%{version}.tar.gz
BuildRoot:   %{_tmppath}/%{name}-%{version}-root
URL: 	     http://www.gnome.org/

%description
ggv allows you to view PostScript documents, and print ranges
of pages.

%prep
%setup

%build

# libtool workaround for alphalinux
%ifarch alpha
  ARCH_FLAGS="--host=alpha-redhat-linux"
%endif

# Needed for snapshot releases.
%configure $ARCH_FLAGS

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS BUGS COPYING ChangeLog MAINTAINERS NEWS README TODO
%{_bindir}/*
%{_datadir}/*


%changelog
* Tue Feb 20 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- removing hard-coded paths, and cleaning macros

* Fri Aug 27 1999 Karl Eichwalder <ke@suse.de>
- Added more %doc files
- Fixed the spelling of PostScript and the Source entry

* Sat Aug 21 1999 Herbert Valerio Riedel <hvr@hvrlab.dhs.org>
- Actualized spec file

* Thu Aug 13 1998 Marc Ewing <marc@redhat.com>
- Initial spec file copied from gnome-graphics
