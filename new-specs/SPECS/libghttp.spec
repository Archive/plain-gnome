%define  ver     1.0.9
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     GNOME http client library
Name:        libghttp
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       X11/gnome
Source:      libghttp-%{ver}.tar.gz
BuildRoot:   %{_tmppath}/ghttp-%{PACKAGE_VERSION}-root
URL:         http://www.gnome.org/

%description
Library for making HTTP 1.1 requests.

%package devel
Summary: GNOME http client development
Group: X11/gnome
Requires: libghttp

%description devel
Libraries and includes files you can use for libghttp development

%changelog

%prep
%setup

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%post 
if ! grep %{_libdir} /etc/ld.so.conf > /dev/null ; then
  echo "%{_libdir}" >> /etc/ld.so.conf
fi
/sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)
%doc AUTHORS COPYING ChangeLog NEWS README doc/ghttp.html
%{_libdir}/lib*.so.*

%files devel
%defattr(-, %fowner, %fgroup)
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/lib*.la
%{_includedir}/*
%{_libdir}/ghttpConf.sh
