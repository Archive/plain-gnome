%define name		bonobo
%define ver		0.37
%define RELEASE		0_plain_0
%define rel		%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Name:		%name
Summary:	Library for components in GNOME
Version: 	%ver
Release: 	%rel
Copyright: 	LGPL
Group:		System Environment/Libraries
Source: 	%{name}-%{ver}.tar.gz
URL: 		http://www.gnome.org/
BuildRoot:	%{_tmppath}/%{name}-%{ver}-root
Requires:	gnome-libs >= 1.2.5
Requires:	ORBit >= 0.5.4
Requires:	oaf >= 0.5.1
Requires:	libxml >= 1.8.10

%description
Bonobo is a library that provides the necessary framework for GNOME
applications to deal with components, i.e. those with a
spreadsheet and graphic embedded in a word-processing document.

%package devel
Summary:	Libraries and include files for the Bonobo component model
Group:		Development/Libraries
Requires:	%name = %{PACKAGE_VERSION}

%description devel
This package provides the necessary development libraries and include
files to allow you to develop programs using the Bonobo component model.

%prep
%setup

%build
LC_ALL=""
LINGUAS=""
LANG=""
export LC_ALL LINGUAS LANG

%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%post
if ! grep %{_libdir} /etc/ld.so.conf > /dev/null ; then
  echo "%{_libdir}" >> /etc/ld.so.conf
fi
  
/sbin/ldconfig
  
%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README
# %config %{_sysconfdir}/CORBA/servers/*.gnorba
%{_bindir}/*
%{_libdir}/bonobo
%{_libdir}/pkgconfig/libefs.pc
%{_libdir}/*.sh
%{_libdir}/lib*.so.*
%{_datadir}/bonobo
%{_datadir}/idl/*.idl
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/mime-info/*.keys
%{_datadir}/oaf/*.oaf

%files devel
%defattr(-, %fowner, %fgroup)
%{_includedir}/bonobo
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/lib*.la
%{_includedir}/*.h


%changelog
* Wed Feb 21 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixed macros and hard-coded paths.  Should be very portable now.

* Wed Oct 18 2000 Eskil Heyn Olsen <eskil@eazel.com>
- Added requirements to the base package
- Added bonobo-ui-extract to the file list of the base pacakge

* Tue Feb 22 2000 Jens Finke <jens.finke@informatik.uni-oldenburg.de>
- Added bonobo.h to the file list of devel package.

* Wed Nov 10 1999 Alexander Skwar <ASkwar@DigitalProjects.com>
- Updated to version 0.5
- fixed spec file
- Un-quiet things
- stripped binaries
- unsetted language environment variables

* Sat Oct 2 1999 Gregory McLean <gregm@comstar.net>
- Updated the spec for version 0.4
- Updated the files section.

* Sun Aug 1 1999 Gregory McLean <gregm@comstar.net>
- Some updates. sysconfdir stuff, quiet down the prep/configure stage.

* Sat May 1 1999 Erik Walthinsen <omega@cse.ogi.edu>
- created spec file
