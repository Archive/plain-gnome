%define  ver     1.2.9
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     Handy library of utility functions
Name:        glib
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       Libraries
Source:      glib-%{ver}.tar.gz
BuildRoot:   %{_tmppath}/glib-%{PACKAGE_VERSION}-root
URL:         http://www.gtk.org

%description
Handy library of utility functions.  Development libs and headers
are in glib-devel.

%package devel
Summary:   GIMP Toolkit and GIMP Drawing Kit support library
Group:     X11/Libraries

%description devel
Static libraries and header files for the support library for the GIMP's X
libraries, which are available as public libraries.  GLIB includes generally
useful data structures.

%prep
%setup

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, %fowner, %fgroup)

%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/libglib-1.2.so.*
%{_libdir}/libgthread-1.2.so.*
%attr(755,-,-) %{_libdir}/libgmodule-1.2.so.*
%{_libdir}/pkgconfig/*

%files devel
%defattr(-, %fowner, %fgroup)

%{_libdir}/lib*.so
%{_libdir}/*a
%{_libdir}/glib
%{_includedir}/*
%{_mandir}/man1/*
%{_datadir}/aclocal/*
%{_bindir}/*
%{_infodir}/glib.*

%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixed hard-coded paths and macros

* Thu Feb 11 1999 Michael Fulbright <drmike@redhat.com>
- added libgthread to file list

* Fri Feb 05 1999 Michael Fulbright <drmike@redhat.com>
- version 1.1.15

* Wed Feb 03 1999 Michael Fulbright <drmike@redhat.com>
- version 1.1.14

* Mon Jan 18 1999 Michael Fulbright <drmike@redhat.com>
- version 1.1.13

* Wed Jan 06 1999 Michael Fulbright <drmike@redhat.com>
- version 1.1.12

* Wed Dec 16 1998 Michael Fulbright <drmike@redhat.com>
- updated in preparation for the GNOME freeze

* Mon Apr 13 1998 Marc Ewing <marc@redhat.com>
- Split out glib package
