%define  ver     0.12
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     Default GTK+ theme engines
Name:        gtk-engines
Version:     %ver
Release:     %rel
Copyright:   GPL
Group:       X11/Libraries
Source:      gtk-engines-%{PACKAGE_VERSION}.tar.gz
URL:         http://gtk.themes.org/
BuildRoot:   %{_tmppath}/gtk-engines-%{PACKAGE_VERSION}-root

%description
These are the graphical engines for the various GTK+ toolkit themes.
Included themes are:

  - Notif
  - redmond95
  - Pixmap
  - Metal (Java swing-like)

%prep
%setup 

%build
%configure
%make

%install
%setupbuildroot
%makeinstall

%clean
%cleanbuildroot

%files
%defattr(-, %fowner, %fgroup)
%doc COPYING README ChangeLog
%{_libdir}/gtk/themes/engines
%{_datadir}/themes/Pixmap
%{_datadir}/themes/Metal
%{_datadir}/themes/Notif
%{_datadir}/themes/Redmond95


%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- macro fixups, and removal of hard-coded paths

* Fri Nov 20 1998 Michael Fulbright <drmike@redhat.com>
- First try at a spec file
