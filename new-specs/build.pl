#!/usr/bin/perl

# This is a program for automatically building and installing
# a list of packages.  This is especially useful for building
# and testing large suites of packages where some have
# BuildPreReqs on others, such as Gnome-1.4.
#
# --jurgen@botz.org, 4/2001, (c) Jurgen Botz, GPL

use Getopt::Long;
use File::Find;

# very useful for debugging...
#use Data::Dumper;

# ------------------------------------------------------------------------
# globals

# defaults for options
%OPTS = ( release      => "",
	  build        => 1,
	  install      => 0,
	  target       => "",
	  define       => [],
	  topdir       => ($_ = `pwd`, chomp, $_),
	  tmpdir       => "",
	  logdir       => "",
	  specsdir     => "SPECS",
	  listfile     => "",
	  rebuild      => 0,
	  clean        => 1,
	  continue     => 0,
	  oldpackage   => 0,
	  replacefiles => 1,
	  deps         => 1,
	  forceinstall => 0,
	  verbose      => 0,
	  debug        => 0
        );

# constants
$MAX_MACRO_DEPTH = 32;
$RPMCMD  = "rpm";
$RPMOPTS = "";

$| = 1;

main();

exit 0;

# ------------------------------------------------------------------------
# Useage

sub Useage() {
    print <<"EOT";
Useage: build [options] ... [specfile|packagename] ...

This build script takes a list of either rpm spec files or package names,
and tries to build and install all packages in the specified order.  The
list can be provided as argument or in a file (one package/spec per line)
with the --listfile option, but not both.  Options with args can be 
given as '--opt <arg>' or '--opt=<arg>'.

To use the --install flag you probably need to be root (unless you
have write perm to the RPM db and all dirs in all target file systems).

General options:
    --topdir <dir>     - root of rpm build environment
    --specsdir <dir>   - directory containing spec files
    --listfile <file>  - list of packages or spec files to build in order
    --release <string> - override RPM release parameter (only works if
                         spec file is written appropriately for this)
    --target <string>  - build target (rpm --target)
    --define '<m> <s>' - define a macro 'm' to expand to 's' (this option
		         can be given multiple times)
    --(no)continue     - continue to next pkg after error? (default no)
    --(no)verbose      - extra output (on stdout)? (default no)
    --(no)debug        - debugging output (on stderr)? (default no)

Build options:
    --(no)build        - to build or not to build? (default yes)
    --(no)rebuild      - build even if there is already an SRPM of this
	                 release under <topdir>/SRPMS? (default no)
    --(no)clean        - rm -rf $RPM_BUILD_ROOT after build? (default yes)
                         (only if spec file uses %cleanbuildroot macro)
Install options:
    --(no)install      - to install or not to install? (default no)
    --(no)oldpackage   - "upgrade" to older packages? (default no)
    --(no)replacefiles - replace files in case of conflict? (default yes)
    --(no)deps         - honor or ignore dependencies when installing?
                         (rpm --nodeps)(default yes)
    --forceinstall     - same as '--install --oldpackage --nodeps
                         --replacefiles'
Examples:
    Build & install everything from file ./build.list in order, but
    stop if anything fails.  Install even if there are dependency
    problems or file conflicts or if an already installed package
    appears "newer".  You'd want to do something like this to
    i.e. build gnome 1.4 on a system that has gnome 1.2 on it.

    \$ $0 --listfile ./build.list --topdir=~/rpmbuild --forceinstall

   Build (but don't install) all spec files in the SPECS subdir, 
   continuing to the next package on most errors.  Override the
   release in the spec file with "1a".

    \$ $0 --release="1a" --target=i686 --continue SPECS/*.spec

EOT
}


# ------------------------------------------------------------------------
# make sure needed directories exist

sub verifyDirs {

    my $top = $OPTS{topdir};

    chdir( $top ) || die "$OPTS{topdir} (topdir) not found";
    -d "SPECS"    || mkdir( "SPECS", 0755 );
    -d "SOURCES"  || mkdir( "SOURCES", 0755 );
    -d "BUILD"    || mkdir( "BUILD", 0755 );
    -d "RPMS"     || mkdir( "RPMS", 0755 );
    -d "SRPMS"    || mkdir( "SRPMS", 0755 );
    $OPTS{tmpdir} && ( -d $OPTS{tmpdir}  || mkdir( $OPTS{tmpdir}, 0755 ));
    $OPTS{logdir} && ( -d $OPTS{logdir}  || mkdir( $OPTS{tmpdir}, 0755 ));
}

# set RPM options from our options

sub setRpmOpts {
    if ( $OPTS{release} ) { $RPMOPTS .= "--define 'CUSTOM_RELEASE $OPTS{release}' "; }
    if ( ! $OPTS{clean} ) { $RPMOPTS .= "--define 'cleanbuildroot echo not cleaning' "; }
    if ( $OPTS{topdir} )  { $RPMOPTS .= "--define '_topdir $OPTS{topdir}' "; }
    if ( $OPTS{tmpdir} )  { $RPMOPTS .= "--define '_tmpdir $OPTS{tmpdir}' "; }
    if ( $OPTS{target} )  { $RPMOPTS .= "--target=$OPTS{target} "; }
    foreach ( @{$OPTS{define}} ) {
                            $RPMOPTS .= "--define '$_' ";
    }

}


# ------------------------------------------------------------------------
# MAIN PROGRAM
# ------------------------------------------------------------------------

sub main {

    my @pkglist = ();
    my $s;
    my $l, $longest;
    my $res;

    # command line processing & initialization
    &GetOptions( \%OPTS, "install!", "release=s", "target=s",
		 "topdir=s", "listfile=s", "specsdir=s",
		 "tmpdir=s", "logdir=s", "define=s@", "build!",
		 "rebuild!", "continue!", "verbose!", "debug!",
		 "oldpackage!", "replacefiles!", "deps!", "clean!",
		 "forceinstall" => sub { $OPTS{install} = 1;
					 $OPTS{deps} = 0;
				         $OPTS{oldpackage} = 1;
					 $OPTS{replacefiles} = 1; },
		 "help" => sub { &Useage; exit 0; }
	       );

    # we need either a listfile OR non-options args
    if (( $#ARGV == -1 && ! $OPTS{listfile} ) || ( $#ARGV >= 0 && $OPTS{listfile} )) {
	&Useage; exit 1;
    };

    &verifyDirs;
    &setRpmOpts;
    &getRpmMacros;

    # real work starts here
    getSpecs( \@pkglist ) || die "couldn't read pkg info from spec files";

    # just so we can line up all the OK/FAILEDs
    map { $l = length( $_->{name} . $_->{ver} );
	  $longest = $l if ($l > $longest); } @pkglist;
    $longest += 12 + 2;

    foreach $pkg ( @pkglist ) {

	if ( $OPTS{build} ) {                       #1234567890
	    $s = $pkg->{name} . "-" . $pkg->{ver} . ": building";
	    $l = length( $s );
	    print $s . "." x ($longest - $l) . "..";

	    $res = buildPkg( $pkg );
	    
	    print "$res\n";
	    exit 1 if ( $res eq "FAILED" && ! $OPTS{continue} );
	}

	if ( $OPTS{install} ) {                     #123456789012
	    $s = $pkg->{name} . "-" . $pkg->{ver} . ": installing";
	    $l = length( $s );
	    print $s . "." x ($longest - $l) . "..";

	    $res = installPkg( $pkg );
	    
	    print "$res\n";
	    exit 1 if ( $res eq "FAILED" && ! $OPTS{continue} );
	}
    }
}


# ------------------------------------------------------------------------
# run rpm on a dummy spec file with a "%dump" in it and snarf the 
# macros into %RPM_MACROS.  this is better than "rpm --showrc" 
# because it picks up things like --target=<arch>, and other things
# rpm does behind the scenese when actually processing a spec file.

sub getRpmMacros {
    
    my ( $name, $args, $body );
    my $text;

    if ( $OPTS{verbose} ) { print "reading rpm macros...\n"; }
    $tmpfile = newtmpfile();

    ( $text = <<EOT ) =~ s/^\s*//mg;
    Summary: dummy, dum, dum
    Name: dummy
    Version: 0.0.1
    Release: 1
    Source: dummy_src.tar.gz
    Group: Applications
    License: GPL

    %description
    This is a dummy

    %prep
    %dump
    %setup
EOT
    open( TMPF, ">$tmpfile" )      || die "couldn't open temp file $tmpfile: $!";
    print TMPF $text;
    close( TMPF );

    $cmd = "$RPMCMD $RPMOPTS -bp $tmpfile 2>&1";
    print STDERR "$cmd\n"                     if $OPTS{debug};
    open( RPM, "$cmd |" )                     || die "couldn't run $cmd: $!";
    while ( <RPM> ) { /^===/ && last; }       # skip junk at begnning
    while ( <RPM> ) {
	if ( /^\s*(-?\d+)[:=] (\w+)(\([^\)]*\))?\s*(.*)/ ) {
	    $name = $2; $body = $4;	             # $args = $3, $level = $1;
	    $RPM_MACROS{$name} = $body;
	} elsif ( $name && ! /^===/ ) {
	    # append to multi-line maro.  move \n to beginning of
	    # line because we didn't match it above and we don't
	    # want one at the very end
	    chomp; $RPM_MACROS{$name} .= "\n" . $_;
	} else {
	    last;
	}    
    }
    close( RPM );
    unlink( $tmpfile );
}


# ------------------------------------------------------------------------
# read all the spec files and make a %pkg for each one

sub getSpecs {
    my ($specs) = @_;

    my $specfile;
    my @pkgs;

    # get list of packages/specs from file or ARGV
    if ( my $listfile = $OPTS{listfile} ) {
	unless ( -f $listfile )       {   die "couldn't find $listfile; $!"; }
	open( LF, $listfile )         ||  die "couldn't open $listfile; $!";

	while ( <LF> ) {
	    chomp; s/^\s*(\S+).*/$1/;
	    next if /^#/;
	    push( @pkgs, $_ );
	}
    } elsif ( $#ARGV >= 0 ) {
	push( @pkgs, @ARGV );
    } else {
	return 0;
    }
    close( LF );

    # read them all.  put processed info into @specs.
    foreach ( @pkgs ) {
	if (( /.spec$/ || /\// ) && -f $_ ) {
	    $specfile = $_;
	} else {
	    $specfile = "${OPTS{specsdir}}/$_.spec";
	    unless ( -f $specfile ) { die "couldn't find specfile for $_"; }
	}
	push( @$specs, getPkgInfo( $specfile ));
    }

    return 1;
} 


# ------------------------------------------------------------------------
# get the name, version, release and subpackages from the spec file
#
# includes a very simple RPM macro processor; this is not a 'correct'
# implementation of the way rpm processes macros, but for getting
# the values of the spec file tags we need it should be enough for now.

# First some functions we'll need.  Why can't perl nest function defns?
# "use packages" they say... yeah, yeah, but I want access to lexical
# vars in the outer scope without breaking encapsulation for that scope!
# So what we do instead is cheat by declaring those 'local' below...

    # ---------------------------
    # process a tag
    sub tag {
	my ($name, $val, $field) = @_;
	
	$pkg->{$field} = $val if $field;

	# rpm auto-defines a macro with lowercase tag name
	$name =~ tr/A-Z/a-z/;
	defm( $name, $val ) unless ( $macros{$name} );

	# ...and in addition rpm defines these...
	$_ = $name;
	/version/ && ! $macros{'PACKAGE_VERSION'} && defm( 'PACKAGE_VERSION', $val);
	/release/ && ! $macros{'PACKAGE_RELEASE'} && defm( 'PACKAGE_RELEASE', $val);
    }

    # ---------------------------
    # define a macro
    sub defm {
	my ($name, $body) = @_;

	# rpm expands body at invocation, not defn
	# but this is easier...
	$body = expm( $body );
	$body =~ s/^\s+//; s/\s+$//;
	$macros{$name} = $body if $body;
    }

    # ---------------------------
    # expand macros
    sub expm {
	my ($line) = @_;

	my ($s,, $neg);

	# process line until no % or we've done it $MAX_MACRO_DEPTH times
	$_ = $line;
	for ( $i=0; /%/ && $i < $MAX_MACRO_DEPTH; $i++ ) {
	    # expand simple macros
	    if ( /%(\w+)/ )      { $s = $macros{$1}; s/%\w+/$s/     if $s; }
	    if ( /%\{(\w+)\}/  ) { $s = $macros{$1}; s/%\{\w+\}/$s/ if $s; }
	    # expand conditionals
	    if ( /%\{(\!)*\?(\w+)(:\w+)?\}/ ) {
		$neg = length( $1 ) % 2;
		$s = $macros{$2};
		if (( $s && ! $neg ) || ( ! $s && $neg )) {
		    (( $s = $3 ) =~ s/^:// ) if $3;
		} else {
		    $s = "";
		}
		s/%\{[^\}]+\}/$s/;
	    }
	}
	s/^\s+//; s/\s+$//;        # strip lead/trail space, incl \n
	return $_;
    }

# ---------------------------
# main sub getPkgInfo

sub getPkgInfo {
    my ($specfile) = @_;

    my ($name, $flag, $define, $buildarch);
    my %pkg;
    my $pkgtop;

    local $pkg = \%pkg;
    local %macros = %RPM_MACROS;   # we'll work with a copy of the macro table

    # initialize arch
    $pkg->{arch} = $macros{_target_cpu};

    open( SF, $specfile )          || die "couldn't open $specfile: $!";
    while ( <SF> ) {

	next if /^\s*\#/;          # skip comment-only lines
	s/([^\\])*\#.*$/$1/;       # strip end of line comments
	s/^\s*//; s/\s*$//;        # strip leading/trailing whitespace

	# process %define
	if ( /%define\s+(\w+)\s+(.*)$/ ) {
	    defm( $1, $2 );
	    next;
	}
	# expand macros
	if ( /%/ ) {
	    $_ = expm( $_ );
	}

	print "$_\n"               if $OPTS{debug};

	# get the interesting tags...
	/^(Name):\s*(.*)/          && tag( $1, $2, "name" );
	/^(Version):\s*(.*)/       && tag( $1, $2, "ver" );
	/^(Release):\s*(.*)/       && tag( $1, $2, "rel" );
	/^(BuildArch\w*):\s*(.*)/  && tag( $1, $2, "arch" );

	# we don't actually use the rest of these, but we might need 
	# the auto-macros they create...
	/^(Summary):\s*(.*)/       && tag( $1, $2 );
	/^(Copyright):\s*(.*)/     && tag( $1, $2 );
	/^(Group):\s*(.*)/         && tag( $1, $2 );
	/^(Source\d*):\s*(.*)/     && tag( $1, $2 );
	/^(Patch\d*):\s*(.*)/      && tag( $1, $2 );
	/^(BuildRoot):\s*(.*)/     && tag( $1, $2 );
	/^(URL):\s*(.*)/           && tag( $1, $2 );
	/^(Packager):\s*(.*)/      && tag( $1, $2 );
	/^(Vendor):\s*(.*)/        && tag( $1, $2 );

	# when we hit a %package section, make everything we've
	# got so far the top and start putting new interesting
	# tags into subpkg structures
	if ( /^\s*%package\s+(-\S\s+)?(\S+)/ ) {
	    $name = $2;
	    $flag = $1 ? $1 : "";  # avoid silly warning

	    # we want a copy of the pkg hash for each sub-package...
	    # (note: this wouldn't work if the structure were 'deep',
	    # but the only reference in the hash is the subpkgs array,
	    # which we only need on top, so we're ok with this trick)
	    my %pkg = %$pkg;

	    if ( ! $pkgtop && $pkg->{name} ) {
		$pkgtop = \%pkg;
	    } elsif ( $pkg->{name} ) {
		push( @{$pkgtop->{subpkgs}}, \%pkg );
	    } else {                              # should be an error
		die "bad spec file";
	    }
	    $pkg->{name} = $pkgtop->{name} . "-$name" if ( ! $flag );
	    $pkg->{name} = $name                      if ( $flag =~ /-n/ );
	}                                           # else result undefined
    }

    # if we haven't got a top level pkg yet, this must be all there is,
    # otherwise push the last subpackage
    if ( ! $pkgtop ) {
	$pkgtop = $pkg unless $pkgtop;
    } else {
	push( @{$pkgtop->{subpkgs}}, $pkg );
    }

    #print "==============\n";
    #print Dumper( $pkgtop );
    #print "==============\n";

    close( SF );
    return $pkgtop;
}


# ------------------------------------------------------------------------
# build a package

sub buildPkg {
    my ($pkg) = @_;

    my $name    = $pkg->{name};
    my $ver     = $pkg->{ver};
    my $rel     = $pkg->{rel};
    my $logfile = getlogfile( $name );

    if ( ! $OPTS{rebuild} && -f "${OPTS{topdir}}/SRPMS/$name-$ver-$rel.src.rpm" ) {
	return "already built";
    }
    appendf( $logfile, "\n" . "=" x 72 . "\n>>> BUILDING\n" );
    
    my $cmd = "$RPMCMD $RPMOPTS -ba ${OPTS{specsdir}}/${name}.spec";
    if ( runcmd( $cmd, $logfile )) {
	return "FAILED";
    }
    return "OK";
}


# ------------------------------------------------------------------------
# install a package

sub installPkg {
    my ($pkgtop) = @_;

    my $subpkg;
    my @rpms;
    my ($name, $ver, $rel, $arch);
    my $logfile = getlogfile( $pkgtop->{name} );

    # find all RPMS to install
    foreach $subpkg ( $pkg, @{$pkg->{subpkgs}} ) {
	$name = $subpkg->{name};
	$ver  = $subpkg->{ver};
	$rel  = $subpkg->{rel};
	$arch = $subpkg->{arch};

	# do we actually have the RPM?
	next unless ( -f "RPMS/$arch/$name-$ver-$rel.$arch.rpm" );
	    
        # see if it's already installed
        $cmd = "$RPMCMD -q $name-$ver-$rel";
        if ( runcmd( $cmd, $logfile )) {
            # if not, then...
            push( @rpms, "RPMS/$arch/$name-$ver-$rel.$arch.rpm" );
	}
    }

    # if there's anything to install, do it...
    if ( @rpms ) {
        appendf( $logfile, "\n" . "=" x 72 . "\n>>> INSTALLING\n" );

        my $cmd = "$RPMCMD -v ";
        if ( $OPTS{oldpackage} )   { $cmd .= "--oldpackage "; } 
        if ( $OPTS{replacefiles} ) { $cmd .= "--replacefiles "; } 
        if ( ! $OPTS{deps} )       { $cmd .= "--nodeps "; } 
        $cmd .= "-U " . join( " ",  @rpms );
        if ( runcmd( $cmd, $logfile )) {
            return "FAILED";
        } else {
            return "OK";
        }
    } else {
        return "already installed or no RPMS";
    }
}


# ------------------------------------------------------------------------
# run a command, with logging

sub runcmd {
    my ($cmd, $logfile) = @_;

    my ($rc, $sig, $txt);

    print "\n$cmd\n" if $OPTS{debug};
    appendf( $logfile, "\n" . "=" x 72 . "\n>>> $cmd\n" );

    # straight from the camel's mouth...
    $rc = 0xffff & system( "$cmd >>$logfile 2>&1 </dev/null" );
    $txt  = sprintf( "\n%#04x", $rc );
    $txt .= sprintf( " <<< $cmd\n", $rc );
    if ( $rc == 0 ) {
	$txt .= "command OK\n"; }
    elsif ( $rc == 0xff00 ) {
	$txt .= "command FAILED to run: $!\n"; }
    elsif ( $rc > 0x80 ) {
	$txt .= "command FAILED; exit status: $rc\n"; }
    else {
	$txt .= "command FAILED with ";
	$sig = $rc;
	if ( $rc & 0x80 ) {
	    $txt .= "coredump, ";
	    $sig &= ~0x80; }
	$txt .= "signal $sig\n";
	appendf( $logfile, $txt );
	print STDERR "\nexit with signal $sig\n";
	exit $rc;
    }
    appendf( $logfile, $txt );
    return $rc;
}


# ------------------------------------------------------------------------
# misc subroutines

sub appendf {
    my ($fn, $txt) = @_;

    open( F, ">>$fn" )            || die "couldn't open $fn for append";
    print F $txt;
    close( F );
}

sub newtmpfile {

    my $tmpdir = $OPTS{tmpdir};
    if ( ! $tmpdir ) {
	$tmpdir = "/var/tmp";
    }
    return "$tmpdir/build_tmp.$$";
}

sub getlogfile {
    my ($name) = @_;

    if ( $OPTS{logdir} ) {
	return "$OPTS{logdir}/$name.log";
    } else {
	return "$name.log";
    }
}

