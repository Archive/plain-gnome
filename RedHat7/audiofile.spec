%define  ver     0.2.1
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     Library to handle various audio file formats
Name:        audiofile
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       Libraries/Sound
Source:      audiofile-%{version}.tar.gz
BuildRoot:   /var/tmp/audiofile-%{version}-root
Obsoletes:   libaudiofile

%description
Library to handle various audio file formats.
Used by the esound daemon.

%package devel
Summary: Libraries, includes, etc to develop audiofile applications
Group: Libraries

%description devel
Libraries, include files, etc you can use to develop audiofile applications.

%prep
%setup

%build
CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%{_prefix} \
    --bindir=%{_bindir} --includedir=%{_includedir} \
    --datadir=%{_datadir} --libdir=%{_libdir} 
make $MAKE_FLAGS

%install

rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT

#
# makefile is broken, sets exec_prefix explicitely.
#
make exec_prefix=$RPM_BUILD_ROOT/%{_prefix} \
    prefix=$RPM_BUILD_ROOT/%{_prefix} bindir=$RPM_BUILD_ROOT%{_bindir} \
    includedir=$RPM_BUILD_ROOT%{_includedir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} \
    libdir=$RPM_BUILD_ROOT%{_libdir} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc COPYING TODO README ChangeLog docs
%{_bindir}/*
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/*.a
%{_includedir}/*
%{_datadir}/aclocal/*


%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- Updated and fixed macros

* Fri Nov 20 1998 Michael Fulbright <drmike@redhat.com>
- First try at a spec file
