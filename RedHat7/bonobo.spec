%define name		bonobo
%define ver		0.37
%define RELEASE		0_plain_0
%define rel		%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Name:		%name
Summary:	Library for components in GNOME
Version: 	%ver
Release: 	%rel
Copyright: 	LGPL
Group:		System Environment/Libraries
Source: 	%{name}-%{ver}.tar.gz
URL: 		http://www.gnome.org/
BuildRoot:	/var/tmp/%{name}-%{ver}-root
Requires:	gnome-libs >= 1.2.5
Requires:	ORBit >= 0.5.4
Requires:	oaf >= 0.5.1
Requires:	libxml >= 1.8.10

%description
Bonobo is a library that provides the necessary framework for GNOME
applications to deal with components, i.e. those with a
spreadsheet and graphic embedded in a word-processing document.

%package devel
Summary:	Libraries and include files for the Bonobo component model
Group:		Development/Libraries
Requires:	%name = %{PACKAGE_VERSION}

%description devel
This package provides the necessary development libraries and include
files to allow you to develop programs using the Bonobo component model.

%prep
%setup

%build
%ifarch alpha
    MYARCH_FLAGS="--host=alpha-redhat-linux"
%endif

LC_ALL=""
LINGUAS=""
LANG=""
export LC_ALL LINGUAS LANG

CFLAGS="$RPM_OPT_FLAGS" ./configure $MYARCH_FLAGS --prefix=%{_prefix} \
    --sysconfdir=%{_sysconfdir} --libdir=%{_libdir} \
    --includedir=%{_includedir} --bindir=%{_bindir} \
    --datadir=%{_datadir}

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make -k
fi

%install
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

make -k prefix=$RPM_BUILD_ROOT%{_prefix} \
    sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} \
    libdir=$RPM_BUILD_ROOT%{_libdir} \
    includedir=$RPM_BUILD_ROOT%{_includedir} \
    bindir=$RPM_BUILD_ROOT%{_bindir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} install


%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%post
if ! grep %{_libdir} /etc/ld.so.conf > /dev/null ; then
  echo "%{_libdir}" >> /etc/ld.so.conf
fi
  
/sbin/ldconfig
  
%postun -p /sbin/ldconfig

%files
%defattr(0555, bin, bin)
%{_bindir}/*
%{_libdir}/pkgconfig/libefs.pc
%{_libdir}/bonobo/plugin/*
%{_libdir}/bonobo/monikers/*
%{_libdir}/*.0
%{_libdir}/*.1
%{_libdir}/*.sh
%{_libdir}/*.so


%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README

%defattr (0444, bin, bin)
# %{_datadir}/bonobo/html/bonobo/*.html
# %{_datadir}/bonobo/html/bonobo/*.sgml
# %{_datadir}/bonobo/html/*.html
# %config %{_sysconfdir}/CORBA/servers/*.gnorba
%{_datadir}/bonobo/html/*.hierarchy
%{_datadir}/bonobo/html/*.sgml
%{_datadir}/bonobo/html/*.signals
%{_datadir}/bonobo/html/*.txt
%{_datadir}/bonobo/html/*.types
%{_datadir}/idl/*.idl
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/mime-info/*.keys
%{_datadir}/oaf/*.oaf

%files devel

%defattr(0555, bin, bin)
%dir %{_includedir}/bonobo
%{_libdir}/*.a
%{_libdir}/*.la

%defattr(0444, bin, bin)
%{_includedir}/*.h
%{_includedir}/bonobo/*.h


%changelog
* Wed Feb 21 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixed macros and hard-coded paths.  Should be very portable now.

* Wed Oct 18 2000 Eskil Heyn Olsen <eskil@eazel.com>
- Added requirements to the base package
- Added bonobo-ui-extract to the file list of the base pacakge

* Tue Feb 22 2000 Jens Finke <jens.finke@informatik.uni-oldenburg.de>
- Added bonobo.h to the file list of devel package.

* Wed Nov 10 1999 Alexander Skwar <ASkwar@DigitalProjects.com>
- Updated to version 0.5
- fixed spec file
- Un-quiet things
- stripped binaries
- unsetted language environment variables

* Sat Oct 2 1999 Gregory McLean <gregm@comstar.net>
- Updated the spec for version 0.4
- Updated the files section.

* Sun Aug 1 1999 Gregory McLean <gregm@comstar.net>
- Some updates. sysconfdir stuff, quiet down the prep/configure stage.

* Sat May 1 1999 Erik Walthinsen <omega@cse.ogi.edu>
- created spec file
