%define  ver     0.11
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     Default GTK+ theme engines
Name:        gtk-engines
Version:     %ver
Release:     %rel
Copyright:   GPL
Group:       X11/Libraries
Source:      gtk-engines-%{PACKAGE_VERSION}.tar.gz
URL:         http://gtk.themes.org/
BuildRoot:   /var/tmp/gtk-engines-%{PACKAGE_VERSION}-root

%description
These are the graphical engines for the various GTK+ toolkit themes.
Included themes are:

  - Notif
  - redmond95
  - Pixmap
  - Metal (Java swing-like)

%prep
%setup 

%build
# Needed for snapshot releases.
if [ ! -f configure ]; then
    CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%{_prefix} \
	--libdir=%{_libdir} --datadir=%{_datadir}
fi

CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%{_prefix} \
    --libdir=%{_libdir} --datadir=%{_datadir}

if [ "$SMP" != "" ]; then
  make -j$SMP "MAKE=make -j$SMP"
else
  make
fi

%install

rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT


# makefile is broken, sets exec_prefix explicitely.

make exec_prefix=$RPM_BUILD_ROOT/%{prefix} \
    prefix=$RPM_BUILD_ROOT/%{prefix} libdir=$RPM_BUILD_ROOT%{_libdir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} install 

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc COPYING README ChangeLog
%{_libdir}/gtk/themes/engines/*
%{_datadir}/themes/Pixmap/*
%{_datadir}/themes/Metal/*
%{_datadir}/themes/Notif/*
%{_datadir}/themes/Redmond95/*


%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- macro fixups, and removal of hard-coded paths

* Fri Nov 20 1998 Michael Fulbright <drmike@redhat.com>
- First try at a spec file
