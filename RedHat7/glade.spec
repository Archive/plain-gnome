%define  ver     0.6.0
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     Gtk+ GUI builder
Name:        glade
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       Development/Tools
Source:      glade-%{ver}.tar.gz
BuildRoot:   /var/tmp/glade-%{ver}-root
URL:         http://www.gnome.org/

%description
Glade is a GUI builder for Gtk.

%prep
%setup

%ifarch alpha
  ARCH_FLAGS="--host=alpha-redhat-linux"
%endif

if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh $ARCH_FLAGS --prefix=%{_prefix} \
    --sysconfdir=%{_sysconfdir} --bindir=%{_bindir} \
    --datadir=%{_datadir}
fi

CFLAGS="$RPM_OPT_FLAGS" ./configure $ARCH_FLAGS --prefix=%{_prefix} \
    --sysconfdir=%{_sysconfdir} --bindir=%{_bindir} \
    --datadir=%{_datadir}


%build

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{_prefix} \
    PACKAGE_PIXMAPS_DIR=$RPM_BUILD_ROOT%{_datadir}/pixmaps \
    sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} \
    bindir=$RPM_BUILD_ROOT%{_bindir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} install

%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README

%{_bindir}/glade
%{_datadir}/locale/*/*/*.mo
%{_datadir}/glade/gnome/*
%{_datadir}/glade/gtk/*
%{_datadir}/gnome/apps/Development/glade.desktop
%{_datadir}/pixmaps/*

%changelog
* Wed Feb 21 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- macro fixes, removal of hard-coded paths

* Thu Jul 22 1999 Herbert Valerio Riedel <hvr@hvrlab.dhs.org>
- changed configure options in order to build on all alphas

* Wed Jun 23 1999 Jose Mercado <jmercado@mit.edu>
- Changed the Source variable to use %{var}.
- Fixed glade.desktop's path so rpm will find it.
