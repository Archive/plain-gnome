# Note that this is NOT a relocatable package
%define name		oaf
%define ver		0.6.4
%define RELEASE		0_plain_0
%define rel		%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Name:		%name
Summary:	Object activation framework for GNOME
Version: 	%ver
Release: 	%rel
License: 	LGPL and GPL
Group:		System Environment/Libraries
Source: 	%{name}-%{ver}.tar.gz
Patch0:         oaf-perl-configure.patch
URL: 		http://www.gnome.org/
BuildRoot:	/var/tmp/%{name}-%{ver}-root

%description
OAF is an object activation framework for GNOME. It uses ORBit.

%package devel
Summary:	Libraries and include files for OAF
Group:		Development/Libraries
Requires:	%name = %{PACKAGE_VERSION}
Obsoletes:	%{name}-devel

%description devel

%prep
%setup
%patch -p1

%build
%ifarch alpha
	MYARCH_FLAGS="--host=alpha-redhat-linux"
%endif

LC_ALL=""
LINGUAS=""
LANG=""
export LC_ALL LINGUAS LANG

CFLAGS="$RPM_OPT_FLAGS" ./configure $MYARCH_FLAGS \
    --enable-more-warnings --prefix=%{_prefix} \
    --sysconfdir=%{_sysconfdir} --bindir=%{_bindir} \
    --libdir=%{_libdir} --includedir=%{_includedir} \
    --datadir=%{_datadir}

make -k

%install
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

make -k sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} \
    prefix=$RPM_BUILD_ROOT%{_prefix} bindir=$RPM_BUILD_ROOT%{_bindir} \
    libdir=$RPM_BUILD_ROOT%{_libdir} \
    includedir=$RPM_BUILD_ROOT%{_includedir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} install


%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%post
if ! grep %{_prefix}/lib /etc/ld.so.conf > /dev/null ; then
	echo "%{_libdir}" >> /etc/ld.so.conf
fi
  
/sbin/ldconfig
  
%postun -p /sbin/ldconfig

%files
%defattr(0555, bin, bin)

%doc AUTHORS COPYING ChangeLog NEWS README
%config %{_sysconfdir}/oaf/*.sample
%config %{_sysconfdir}/oaf/*.xml
%{_bindir}/oaf-client
%{_bindir}/oaf-config
%{_bindir}/oaf-run-query
%{_bindir}/oaf-slay
%{_bindir}/oaf-sysconf
%{_bindir}/oafd
%{_libdir}/*.0
%{_libdir}/*.sh
%{_libdir}/*.so

%defattr (0444, bin, bin)
%{_datadir}/idl/*.idl
%{_datadir}/locale/da/LC_MESSAGES/*.mo
%{_datadir}/locale/de/LC_MESSAGES/*.mo
%{_datadir}/locale/no/LC_MESSAGES/*.mo
%{_datadir}/locale/ru/LC_MESSAGES/*.mo
%{_datadir}/locale/tr/LC_MESSAGES/*.mo
%{_datadir}/oaf/*.oafinfo

%files devel

%defattr(0555, bin, bin)
%dir %{_includedir}/liboaf
%{_libdir}/*.la

%defattr(0444, bin, bin)
%{_includedir}/liboaf/*.h
%{_datadir}/aclocal/*.m4


%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixes and updates

* Tue Aug 29 2000 Maciej Stachowiak <mjs@eazel.com>
- corrected Copyright field and renamed it to License

* Sun May 21 2000 Ross Golder <rossigee@bigfoot.com>
- created spec file (based on bonobo.spec.in)
