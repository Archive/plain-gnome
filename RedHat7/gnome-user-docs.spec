%define version	0.99
%define RELEASE	0_plain_0
%define rel	%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:        GNOME User Documentation
Name:           gnome-user-docs
Version:        %{version}
Release:        %{rel}
Copyright:      FDL
Distribution:   GNOME RPMS
Source:         gnome-user-docs-%{version}.tar.gz
Group:          Documentation
BuildArch:      noarch
BuildRoot:      /var/tmp/%{name}-%{version}-root

%description
This package contains the GNOME Glossary, Introduction to GNOME, and a Unix
Primer.

%prep
%setup
%build
./configure --prefix=%{_prefix}
make 

%install
make prefix=$RPM_BUILD_ROOT%{_prefix} install

%clean 
rm -rf $RPM_BUILD_ROOT

%post
which scrollkeeper-update > /dev/null 2>&1 && scrollkeeper-update
exit 0

%postun
which scrollkeeper-update > /dev/null 2>&1 && scrollkeeper-update
exit 0

%files
%defattr(-, root, root)
%{_prefix}/*

%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- de-uglification, and fixed the macros.

* Mon Nov 27 2000 Kenny Graunke <kwg@teleport.com>
- Initial cut
