# Note that this is NOT a relocatable package
%define  ver     1.2.9pre2
%define  RELEASE 0_plain_1
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     The Gimp Toolkit
Name:        gtk+
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       X11/Libraries
Source:      gtk+-%{ver}.tar.gz
BuildRoot:   /var/tmp/gtk-%{PACKAGE_VERSION}-root
Obsoletes:   gtk
URL:         http://www.gtk.org
Prereq:      /sbin/install-info
Requires:    glib

%description
The X libraries originally written for the GIMP, which are now used by
several other programs as well.

%package devel
Summary:    GIMP Toolkit and GIMP Drawing Kit
Group:      X11/Libraries
Requires:   %{name} = %{version}
Obsoletes:  gtk-devel
PreReq:     /sbin/install-info

%description devel
Static libraries and header files for the GIMP's X libraries, which are
available as public libraries.  GLIB includes generally useful data
structures, GDK is a drawing toolkit which provides a thin layer over
Xlib to help automate things like dealing with different color depths,
and GTK is a widget set for creating user interfaces.
  
%prep
%setup

%build
# Needed for snapshot releases.
if [ ! -f configure ]; then
    CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%{_prefix} \
	--bindir=%{_bindir} --libdir=%{_libdir} --datadir=%{_datadir} \
	--includedir=%{_includedir} --mandir=%{_mandir} \
	--infodir=%{_infodir}
fi

CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%{_prefix} \
    --bindir=%{_bindir} --libdir=%{_libdir} --datadir=%{_datadir} \
    --includedir=%{_includedir} --mandir=%{_mandir} \
    --infodir=%{_infodir}


if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{_prefix} bindir=$RPM_BUILD_ROOT%{_bindir} \
    libdir=$RPM_BUILD_ROOT%{_libdir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} \
    includedir=$RPM_BUILD_ROOT%{_includedir} \
    mandir=$RPM_BUILD_ROOT%{_mandir} \
    infodir=$RPM_BUILD_ROOT%{_infodir} install


%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/gdk.info* %{_infodir}/dir
/sbin/install-info %{_infodir}/gtk.info* %{_infodir}/dir

%preun devel
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/gdk.info* %{_infodir}/dir
    /sbin/install-info --delete %{_infodir}/gtk.info* %{_infodir}/dir
fi

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_libdir}/libgtk-1.2.so.*
%{_libdir}/libgdk-1.2.so.*
%{_datadir}/themes/Default
%{_datadir}/locale/*/*/*
 

%files devel
%defattr(-, root, root)

%{_libdir}/lib*.so
%{_libdir}/*a
%{_includedir}/*
%{_infodir}/*
%{_mandir}/man1/*
%{_datadir}/aclocal/*
%{_bindir}/*


%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- updates, fixes for hard-coded paths, and better macros

* Sun Feb 21 1999 Michael Fulbright <drmike@redhat.com>
- updated spec file

* Sun Oct 25 1998 Shawn T. Amundson <amundson@gtk.org>
- Fixed Source: to point to v1.1 

* Tue Aug 04 1998 Michael Fulbright <msf@redhat.com>
- change %postun to %preun

* Mon Jun 27 1998 Shawn T. Amundson <unknown@unknown.org>
- Changed version to 1.1.0

* Thu Jun 11 1998 Dick Porter <dick@cymru.net>
- Removed glib, since it is its own module now

* Mon Apr 13 1998 Marc Ewing <marc@redhat.com>
- Split out glib package

* Tue Apr  8 1998 Shawn T. Amundson <amundson@gtk.org>
- Changed version to 1.0.0

* Tue Apr  7 1998 Owen Taylor <otaylor@gtk.org>
- Changed version to 0.99.10

* Thu Mar 19 1998 Shawn T. Amundson <amundson@gimp.org>
- Changed version to 0.99.9
- Changed gtk home page to www.gtk.org

* Thu Mar 19 1998 Shawn T. Amundson <amundson@gimp.org>
- Changed version to 0.99.8

* Sun Mar 15 1998 Marc Ewing <marc@redhat.com>
- Added aclocal and bin stuff to file list.
- Added -k to the SMP make line.
- Added lib/glib to file list.

* Fri Mar 14 1998 Shawn T. Amundson <amundson@gimp.org>
- Changed version to 0.99.7

* Fri Mar 14 1998 Shawn T. Amundson <amundson@gimp.org>
- Updated ftp url and changed version to 0.99.6

* Thu Mar 12 1998 Marc Ewing <marc@redhat.com>
- Reworked to integrate into gtk+ source tree
- Truncated ChangeLog.  Previous Authors:
  Trond Eivind Glomsrod <teg@pvv.ntnu.no>
  Michael K. Johnson <johnsonm@redhat.com>
  Otto Hammersmith <otto@redhat.com>
