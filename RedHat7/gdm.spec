%define ver           2.0.98.1
%define rel           0_plain_0
%define localstatedir /var/lib

Summary:     The GNOME Display Manager.
Name:        gdm
Version:     %ver
Release:     %rel
Copyright:   LGPL/GPL
Group:       User Interface/X
Source:      gdm-%{version}.tar.gz
BuildRoot:   /var/tmp/gdm-%{version}-root
Prereq:      /usr/sbin/useradd
Requires:    pam >= 0.68
Requires:    gnome-libs >= 1.0.17

%description
Gdm (the GNOME Display Manager) is a highly configurable
reimplementation of xdm, the X Display Manager. Gdm allows you to log
into your system with the X Window System running and supports running
several different X sessions on your local machine at the same time.

%prep
%setup -q

%build
CFLAGS="-g $RPM_OPT_FLAGS" ./configure --localstatedir=%{localstatedir} \
    --prefix=%{_prefix} --sysconfdir=%{_sysconfdir}/X11 \
    --bindir=%{_bindir} --datadir=%{_datadir}
 make

%install
rm -rf $RPM_BUILD_ROOT

/usr/sbin/useradd -r gdm > /dev/null 2>&1 || /bin/true

make sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir}/X11 \
    prefix=$RPM_BUILD_ROOT%{_prefix} bindir=$RPM_BUILD_ROOT%{_bindir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} \
    localstatedir=$RPM_BUILD_ROOT%{localstatedir} install


# install RH specific session files
rm -f $RPM_BUILD_ROOT%{_sysconfdir}/X11/gdm/Sessions/*

install -m 755 config/Default.redhat $RPM_BUILD_ROOT%{_sysconfdir}/X11/gdm/Sessions/Default
install -m 755 config/Gnome $RPM_BUILD_ROOT%{_sysconfdir}/X11/gdm/Sessions/Gnome
install -m 755 config/Failsafe.redhat $RPM_BUILD_ROOT%{_sysconfdir}/X11/gdm/Sessions/Failsafe
ln -sf Default $RPM_BUILD_ROOT%{_sysconfdir}/X11/gdm/Sessions/default

# change default Init script to be Red Hat default
ln -sf ../../xdm/Xsetup_0 $RPM_BUILD_ROOT%{_sysconfdir}/X11/gdm/Init/Default

# run GiveConsole/TakeConsole
ln -sf ../../xdm/GiveConsole $RPM_BUILD_ROOT%{_sysconfdir}/X11/gdm/PreSession/Default
ln -sf ../../xdm/TakeConsole $RPM_BUILD_ROOT%{_sysconfdir}/X11/gdm/PostSession/Default

# move pam.d stuff to right place
mv $RPM_BUILD_ROOT%{_sysconfdir}/X11/pam.d $RPM_BUILD_ROOT%{_sysconfdir}


%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%pre
/usr/sbin/useradd -u 42 -r gdm > /dev/null 2>&1
# ignore errors, as we can't disambiguate between gdm already existed
# and couldn't create account with the current adduser.
exit 0

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%config %{_sysconfdir}/pam.d/gdm
%config %{_sysconfdir}/X11/gdm/gnomerc
%config %{_sysconfdir}/X11/gdm/gdm.conf
%config %{_sysconfdir}/X11/gdm/locale.alias
%config %{_sysconfdir}/X11/gdm/Sessions/*
%config %{_sysconfdir}/X11/gdm/Init/*
%config %{_sysconfdir}/X11/gdm/PreSession/*
%config %{_sysconfdir}/X11/gdm/PostSession/*
%{_datadir}/gdmconfig/gdmconfig.glade
%{_datadir}/gnome/apps/System/gdmconfig.desktop
%{_datadir}/locale/*/*/*
%{_datadir}/pixmaps/*
%attr(750, gdm, gdm) %dir %{localstatedir}/gdm

%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- %define localstatedir /var/lib and related changes

* Tue Feb 20 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- macro cleanups

* Fri Feb 03 2001 George Lebl <jirka@5z.com>
- Add gdmconfig files

* Fri Feb 02 2001 George Lebl <jirka@5z.com>
- Remove all the patches and do the voodoo that I don't do so well
  to make this thingie work with 2.0.97.1

* Fri Feb 04 2000 Havoc Pennington <hp@redhat.com>
- Modify Default.session and Failsafe.session not to add -login option to bash
- exec the session scripts with the user's shell with a hyphen prepended
- doesn't seem to actually work yet with tcsh, but it doesn't seem to 
  break anything. needs a look to see why it doesn't work

* Fri Feb 04 2000 Havoc Pennington <hp@redhat.com>
- Link PreSession/Default to xdm/GiveConsole
- Link PostSession/Default to xdm/TakeConsole

* Fri Feb 04 2000 Havoc Pennington <hp@redhat.com>
- Fix the fix to the fix (8877)
- remove docs/gdm-manual.txt which doesn't seem to exist from %doc

* Fri Feb 04 2000 Havoc Pennington <hp@redhat.com>
- Enhance 8877 fix by not deleting the "Please login" 
  message

* Fri Feb 04 2000 Havoc Pennington <hp@redhat.com>
- Try to fix bug 8877 by clearing the message below 
  the entry box when the prompt changes. may turn 
  out to be a bad idea.

* Mon Jan 17 2000 Elliot Lee <sopwith@redhat.com>
- Fix bug #7666: exec Xsession instead of just running it

* Mon Oct 25 1999 Jakub Jelinek <jakub@redhat.com>
- Work around so that russian works (uses koi8-r instead
  of the default iso8859-5)

* Tue Oct 12 1999 Owen Taylor <otaylor@redhat.com>
- Try again

* Tue Oct 12 1999 Owen Taylor <otaylor@redhat.com>
- More fixes for i18n

* Tue Oct 12 1999 Owen Taylor <otaylor@redhat.com>
- Fixes for i18n

* Fri Sep 26 1999 Elliot Lee <sopwith@redhat.com>
- Fixed pipewrite bug (found by mkj & ewt).

* Fri Sep 17 1999 Michael Fulbright <drmike@redhat.com>
- added requires for pam >= 0.68

* Fri Sep 10 1999 Elliot Lee <sopwith@redhat.com>
- I just update this package every five minutes, so any recent changes are my fault.

* Thu Sep 02 1999 Michael K. Johnson <johnsonm@redhat.com>
- built gdm-2.0beta2

* Mon Aug 30 1999 Michael K. Johnson <johnsonm@redhat.com>
- built gdm-2.0beta1

* Tue Aug 17 1999 Michael Fulbright <drmike@redhat.com>
- included rmeier@liberate.com patch for tcp socket X connections

* Mon Apr 19 1999 Michael Fulbright <drmike@redhat.com>
- fix to handling ancient gdm config files with non-standard language specs
- dont close display connection for xdmcp connections, else we die if remote
  end dies. 

* Fri Apr 16 1999 Michael Fulbright <drmike@redhat.com>
- fix language handling to set GDM_LANG variable so gnome-session 
  can pick it up

* Wed Apr 14 1999 Michael Fulbright <drmike@redhat.com>
- fix so certain dialog boxes dont overwrite background images

* Wed Apr 14 1999 Michael K. Johnson <johnsonm@redhat.com>
- do not specify -r 42 to useradd -- it doesn't know how to fall back
  if id 42 is already taken

* Fri Apr 9 1999 Michael Fulbright <drmike@redhat.com>
- removed suspend feature

* Mon Apr 5 1999 Jonathan Blandford <jrb@redhat.com>
- added patch from otaylor to not call gtk funcs from a signal.
- added patch to tab when username not added.
- added patch to center About box (and bring up only one) and ignore "~"
  and ".rpm" files.

* Fri Mar 26 1999 Michael Fulbright <drmike@redhat.com>
- fixed handling of default session, merged all gdmgreeter patches into one

* Tue Mar 23 1999 Michael Fulbright <drmike@redhat.com>
- remove GNOME/KDE/AnotherLevel session scripts, these have been moved to
  the appropriate packages instead.
- added patch to make option menus always active (security problem otherwise)
- added jrb's patch to disable stars in passwd entry field

* Fri Mar 19 1999 Michael Fulbright <drmike@redhat.com>
- made sure /usr/bin isnt in default path twice
- strip binaries

* Wed Mar 17 1999 Michael Fulbright <drmike@redhat.com>
- fixed to use proper system path when root logs in

* Tue Mar 16 1999 Michael Fulbright <drmike@redhat.com>
- linked Init/Default to Red Hat default init script for xdm
- removed logo from login dialog box

* Mon Mar 15 1999 Michael Johnson <johnsonm@redhat.com>
- pam_console integration

* Tue Mar 09 1999 Michael Fulbright <drmike@redhat.com>
- added session files for GNOME/KDE/AnotherLevel/Default/Failsafe
- patched gdmgreeter to not complete usernames
- patched gdmgreeter to not safe selected session permanently
- patched gdmgreeter to center dialog boxes

* Mon Mar 08 1999 Michael Fulbright <drmike@redhat.com>
- removed comments from gdm.conf file, these are not parsed correctly

* Sun Mar 07 1999 Michael Fulbright <drmike@redhat.com>
- updated source line for accuracy

* Fri Feb 26 1999 Owen Taylor <otaylor@redhat.com>
- Updated patches for 1.0.0
- Fixed some problems in 1.0.0 with installation directories
- moved /usr/var/gdm /var/gdm

* Thu Feb 25 1999 Michael Fulbright <drmike@redhat.com>
- moved files from /usr/etc to /etc

* Tue Feb 16 1999 Michael Johnson <johnsonm@redhat.com>
- removed commented-out #1 definition -- put back after testing gnome-libs
  comment patch

* Sat Feb 06 1999 Michael Johnson <johnsonm@redhat.com>
- initial packaging
