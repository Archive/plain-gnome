# Note that this is NOT a relocatable package
%define  ver     1.0.12
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     LibGTop library
Name:        libgtop
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       X11/Libraries
Source:      libgtop-%{ver}.tar.gz
BuildRoot:   /tmp/libgtop-root
URL:         http://www.home-of-linux.org/gnome/libgtop/
Prereq:      /sbin/install-info

%description

A library that fetches information about the running system such as
cpu and memory usage, active processes etc.

On Linux systems, these information are taken directly from the /proc
filesystem while on other systems a server is used to read those
information from /dev/kmem or whatever. 

%package devel
Summary:  Libraries, includes, etc to develop LibGTop applications
Group:    X11/libraries
Requires: %{name} = %{version}

%description devel
Libraries, include files, etc you can use to develop GNOME applications.

%package examples
Summary:  Examples for LibGTop
Group:    X11/libraries
Requires: %{name} = %{version}

%description examples
Examples for LibGTop.

%prep
%setup

%build
# Needed for snapshot releases.
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%{_prefix} \
    --without-linux-table --with-libgtop-inodedb \
    --with-libgtop-examples --with-libgtop-guile --with-libgtop-smp \
    --libdir=%{_libdir} --includedir=%{_includedir} \
    --libexecdir=%{_libexecdir} --datadir=%{_datadir}
else
  %ifarch alpha
    CFLAGS="$RPM_OPT_FLAGS" ./configure --host=alpha-redhat-linux \
      --prefix=%{_prefix} --without-linux-table --with-libgtop-inodedb \
      --with-libgtop-examples --with-libgtop-guile --with-libgtop-smp \
      --libdir=%{_libdir} --includedir=%{_includedir} \
      --libexecdir=%{_libexecdir} --datadir=%{_datadir} 
  %else
    CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%{_prefix} \
      --without-linux-table --with-libgtop-inodedb \
      --with-libgtop-examples  --with-libgtop-guile --with-libgtop-smp \
      --libdir=%{_libdir} --includedir=%{_includedir} \
      --libexecdir=%{_libexecdir} --datadir=%{_datadir}
  %endif
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{_prefix} libdir=$RPM_BUILD_ROOT%{_libdir} \
    includedir=$RPM_BUILD_ROOT%{_includedir} \
    libexecdir=$RPM_BUILD_ROOT%{_libexecdir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} install

rm -fr $RPM_BUILD_ROOT/%{_includedir}/libgtop

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)

%doc RELNOTES-0.25 RELNOTES-1.0 AUTHORS ChangeLog NEWS README
%doc TODO NEWS.old copyright.txt
%doc src/inodedb/README.inodedb

%{_libdir}/lib*.so.*
%{_datadir}/*
%{_bindir}/*

%files devel
%defattr(-, root, root)

%{_libdir}/lib*.so
%{_libdir}/*a
%{_libdir}/*.sh
%{_libdir}/*.def
%{_includedir}/*

%files examples
%defattr(-,root,root)

%{_libexecdir}/libgtop


%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- better macros, and general fixups

* Tue Aug 19 1998 Martin Baulig <martin@home-of-linux.org>
- released LibGTop 0.25.0

* Sun Aug 16 1998 Martin Baulig <martin@home-of-linux.org>
- first version of the RPM
