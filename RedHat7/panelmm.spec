Name:            panelmm
Summary:         C++ bindings for making panel applets
Version:         0.1
Release:         0_plain_0
License:         GPL/LGPL
Source:          panelmm-%{version}.tar.gz
Patch0:          panelmm-configure-fix.patch
Group:           System Environment/Libraries
URL:             http://www.gnome.org/
BuildRoot:       /var/tmp/%{name}-%{version}root
BuildRequires:   gnome-libs-devel, ORBit-devel, gtk+-devel >= 1.2.0
BuildRequires:   gtkmm-devel >= 1.1.5, gnome-core-devel

%description
Panelmm is some stuff to let people write panel applet applets in C++.

%package devel
Summary:    panelmm development libraries
Group:      Development/Libraries
Requires:   %{name} = %{version}

%description devel
This package contains the headers and libraries that programmers will
need to develop programs using panelmm.

%prep
rm -rf $RPM_BUILD_ROOT

%setup
%patch -p1

%build
CFLAGS="$RPM_OPT_FLAGS" CXXFLAGS="$RPM_OPT_FLAGS" ./configure \
    --enable-static --prefix=%{_prefix} --sysconfdir=%{_sysconfdir} \
    --libdir=%{_libdir}

%install
make prefix=$RPM_BUILD_ROOT%{_prefix} \
    sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} \
    libdir=$RPM_BUILD_ROOT%{_libdir} install


%clean
rm -rf $RPM_BUILD_ROOT

%files
%{_libdir}/*so*

%files devel
%{_libdir}/*a
%{_libdir}/*sh
%{_includedir}/*h
%{_includedir}/panel--/*h
%{_includedir}/panel--/private/*h

%changelog
* Sun Mar 04 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- Created panelmm spec file

