%define nam	        gtkmm
%define ver	        1.2.5
%define rel             0_plain_0
%define localstatedir	/var/lib

Release:        %rel
Summary:	A C++ interface for the GTK+ (a GUI library for X).
Name:		gtkmm
Version: 	%ver
Copyright:	LGPL
Group:		System Environment/Libraries
Source: 	%{nam}-%{ver}.tar.gz
URL:		http://lazy.ton.tut.fi/gtk--/gtk--.html
BuildRoot:	/var/tmp/%{nam}-%{ver}-root
Obsoletes:	Gtk--, gtk--
BuildPreReq:    gtk+-devel, glib-devel, libsigc++-devel
Requires:	gtk+, glib, libsigc++

%description
This package provides a C++ interface for GTK+ (the Gimp ToolKit) GUI
library.  The interface provides a convenient interface for C++
programmers to create GUIs with GTK+'s flexible object-oriented framework.
Features include type safe callbacks, widgets that are extensible using
inheritance and over 110 classes that can be freely combined to quickly
create complex user interfaces.

%package devel
Summary: Headers for developing programs that will use Gtk--.
Group: Development/Libraries
Obsoletes: Gtk---devel, gtk---devel
Requires: gtk+-devel, glib-devel, libsigc++-devel

%description devel
This package contains the headers that programmers will need to develop
applications which will use Gtk--, the C++ interface to the GTK+
(the Gimp ToolKit) GUI library.

%prep
%setup -q -n %{nam}-%{ver}

%build
%ifarch alpha
        ARCH_FLAGS="--host=alpha-linux-gnu"
%endif
CFLAGS="$RPM_OPT_FLAGS" CXXFLAGS="$RPM_OPT_FLAGS" ./configure  $ARCH_FLAGS \
    --prefix=%{_prefix} \
    --disable-maintainer-mode \
    --enable-static \
    --enable-shared --enable-docs \
    --sysconfdir=%{_sysconfdir} \
    --localstatedir=%{localstatedir}
make

%install
rm -rf $RPM_BUILD_ROOT
make prefix=$RPM_BUILD_ROOT%{_prefix} \
    sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} \
    localstatedir=$RPM_BUILD_ROOT%{localstatedir} install

# replace examples.conf by a really simple one
echo 'CXXBUILD = g++ -O2 $< -o $@ `gtkmm-config --cflags --libs` ' \
        > examples/examples.conf

# strip down the docs
find docs/ \
\(      -name 'Makefile' -or    \
        -name 'Makefile.in' -or \
        -name 'Makefile.am' -or \
        -name '*.m4' -or        \
        -name 'html' -or        \
        -name 'header' -or      \
        -name '*.h'             \
\)      -exec rm {} \;


%clean
rm -rf $RPM_BUILD_ROOT

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root)
%doc examples/ docs/  AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_bindir}/*
%{_includedir}/*.h
%{_includedir}/gdk--
%{_includedir}/gtk--
%{_libdir}/*.la
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/gtkmm
%{_datadir}/aclocal/*

%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixed macros a bit, and removed hard-coded paths.

* Mon Nov 20 2000 Tim Powers <timp@redhat.com>
- rebuilt to fix bad dir perms

* Wed Nov 15 2000 Tim Powers <timp@redhat.com>
- forgot to add the gtk--.m4 file which is in aclocal. Fixes bug
  #20899

* Wed Sep 13 2000 Tim Powers <timp@redhat.com>
- update to 1.2.3

* Wed Aug 9 2000 Tim Powers <timp@redhat.com>
- added Serial so that we can upgrade from the 6.2 Helix packages

* Mon Jul 24 2000 Prospector <prospector@redhat.com>
- rebuilt

* Sat Jul 22 2000 Tim Powers <timp@redhat.com>
- fixed missing BuildPreReq's of libsigc++/libsigc++-devel et al.

* Mon Jul 10 2000 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Jul 05 2000 Tim Powers <timp@redhat.com>
- removed Docdir line in header, it was breaking our /usr/share/doc location
  for docs

* Mon Jul 03 2000 Tim Powers <timp@redhat.com>
- rebuilt to fix /usr/doc problems and also bug #13153 

* Wed Jun 28 2000 Tim Powers <timp@redhat.com>
- using Helix's spec file since ours was horked
