# Note this is NOT a relocatable thing :)
%define name		xml-i18n-tools
%define ver		0.8.1
%define RELEASE		0_plain_0
%define rel		%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Name:		%name
Summary:	This module contains some utility scripts and assorted auto* magic for internationalizing various kinds of XML files.
Version: 	%ver
Release: 	%rel
Copyright: 	GPL
Group:		Development/Tools
Source: 	%{name}-%{ver}.tar.gz
URL: 		http://nautilus.eazel.com/
BuildRoot:	/var/tmp/%{name}-%{ver}-root
BuildArch:      noarch

%description
** Automatically extracts translatable strings from oaf, glade, bonobo
  ui, nautilus theme and other XML files into the po files.

** Automatically merges translations from po files back into .oaf files
  (encoding to be 7-bit clean). I can also extend this merging
  mechanism to support other types of XML files.

%prep
%setup

%build
%ifarch alpha
  MYARCH_FLAGS="--host=alpha-redhat-linux"
%endif

LC_ALL=""
LINGUAS=""
LANG=""
export LC_ALL LINGUAS LANG

CFLAGS="$RPM_OPT_FLAGS" ./configure $MYARCH_FLAGS --prefix=%{_prefix} \
    --sysconfdir=%{_sysconfdir} --bindir=%{_bindir} \
    --datadir=%{_datadir}

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{_prefix} \
    sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} \
    bindir=$RPM_BUILD_ROOT%{_bindir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} install 

%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%post
  
%postun 

%files

%defattr(0555, bin, bin)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%{_datadir}/xml-i18n-tools/*

%defattr (0444, bin, bin)
%{_datadir}/aclocal/*.m4


%changelog
* Mon Mar 01 2001 Maciej Stachowiak <mjs@eazel.com>
- removed devel subpackage
* Thu Jan 04 2000 Robin * Slomkowski <rslomkow@eazel.com>
- created this thing
