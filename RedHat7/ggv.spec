%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     GNOME PostScript viewer
Name: 	     ggv
Version:     1.0
Release:     %rel
Copyright:   GPL
Group: 	     X11/Utilities
Source:	     %{name}-%{version}.tar.gz
BuildRoot:   /var/tmp/%{name}-%{version}-root
URL: 	     http://www.gnome.org/

%description
ggv allows you to view PostScript documents, and print ranges
of pages.

%prep
%setup

%build

# libtool workaround for alphalinux
%ifarch alpha
  ARCH_FLAGS="--host=alpha-redhat-linux"
%endif

# Needed for snapshot releases.
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh $ARCH_FLAGS --prefix=%{_prefix} \
    --sysconfdir=%{_sysconfdir} --datadir=%{_datadir}
fi

CFLAGS="$RPM_OPT_FLAGS" ./configure $ARCH_FLAGS --prefix=%{_prefix} \
    --sysconfdir=%{_sysconfdir} --datadir=%{_datadir}


if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{_prefix} \
    sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)

%doc AUTHORS BUGS COPYING ChangeLog MAINTAINERS NEWS README TODO
%{_bindir}/*
%{_datadir}/*


%changelog
* Tue Feb 20 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- removing hard-coded paths, and cleaning macros

* Fri Aug 27 1999 Karl Eichwalder <ke@suse.de>
- Added more %doc files
- Fixed the spelling of PostScript and the Source entry

* Sat Aug 21 1999 Herbert Valerio Riedel <hvr@hvrlab.dhs.org>
- Actualized spec file

* Thu Aug 13 1998 Marc Ewing <marc@redhat.com>
- Initial spec file copied from gnome-graphics
