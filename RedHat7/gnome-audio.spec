%define  ver     1.4.0
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:              Sounds for GNOME events.
Name:                 gnome-audio
Version:              %ver
Release:              %rel
Copyright:            Public domain
Group:                System Environment/Libraries
Source:               gnome-audio-%{ver}.tar.gz
BuildRoot:            /var/tmp/gnome-audio-guide-%{PACKAGE_VERSION}-root
URL:                  http://www.gnome.org
BuildArchitectures:   noarch

%package extra
Summary:   foo
Group:     Data/Media/Sound

%description
If you use the GNOME desktop environment, you may want to
install this package of complementary sounds.

%description extra
This package contains extra sound files useful for customizing the
sounds that the GNOME desktop environment makes.

%prep
%setup


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
make prefix=$RPM_BUILD_ROOT/%{_prefix} datadir=$RPM_BUILD_ROOT%{_datadir} install 

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc README
%{_datadir}/sounds/shutdown1.wav
%{_datadir}/sounds/startup3.wav
%{_datadir}/sounds/panel
%{_datadir}/sounds/gtk-events

#symlinks
%{_datadir}/sounds/login.wav
%{_datadir}/sounds/logout.wav

%files extra
%{_datadir}/sounds/card_shuffle.wav
%{_datadir}/sounds/phone.wav


%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixed hard-coded paths, and macros

* Tue Jan 26 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.4

* Thu Dec 17 1998 Michael Fulbright <drmike@redhat.com>
- first pass at a spec file
