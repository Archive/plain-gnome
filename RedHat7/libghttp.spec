%define  ver     1.0.9
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     GNOME http client library
Name:        libghttp
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       X11/gnome
Source:      libghttp-%{ver}.tar.gz
BuildRoot:   /var/tmp/ghttp-%{PACKAGE_VERSION}-root
URL:         http://www.gnome.org/

%description
Library for making HTTP 1.1 requests.

%package devel
Summary: GNOME http client development
Group: X11/gnome
Requires: libghttp

%description devel
Libraries and includes files you can use for libghttp development

%changelog

%prep
%setup
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%{_prefix} \
    --libdir=%{_libdir} --includedir=%{_includedir}
fi

CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%{_prefix} \
    --libdir=%{_libdir} --includedir=%{_includedir}


if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{_prefix} libdir=$RPM_BUILD_ROOT%{_libdir} \
    includedir=$RPM_BUILD_ROOT%{_includedir} install

%clean
rm -rf $RPM_BUILD_ROOT

%post 
if ! grep %{_libdir} /etc/ld.so.conf > /dev/null ; then
  echo "%{_libdir}" >> /etc/ld.so.conf
fi

/sbin/ldconfig

%postun -p /sbin/ldconfig
%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README doc/ghttp.html
%{_libdir}/lib*.so*

%files devel
%defattr(-, root, root)

# %{_libdir}/lib*.so
%{_libdir}/*a
%{_includedir}/*
%{_libdir}/ghttpConf.sh
