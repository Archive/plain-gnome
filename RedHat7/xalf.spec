# Note that this is NOT a relocatable package
%define ver      0.7
%define rel      0_plain_0

Summary:     A utility to provide feedback when starting X11 applications.
Name:        xalf
Version:     %ver
Release:     %rel
Copyright:   GPL
Group:       X11/Utilities
Source:      xalf-%{ver}.tar.gz
BuildRoot:   /var/tmp/xalf-%{PACKAGE_VERSION}-root
URL:         http://www.lysator.liu.se/~astrand/projects/xalf

%description
This is a small utility to provide feedback when starting X11
applications.  Feedback can be given via four different indicators:
An invisible window (to be used in conjunction with a task pager like
Gnomes tasklist_applet or KDE Taskbar), an generic splashscreen, an
hourglass attached to the mouse cursor or an animated star. 

%prep
%setup

%build
CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%{_prefix} \
    --libdir=%{_libdir} --bindir=%{_bindir} --datadir=%{_datadir}
make

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_datadir}/pixmaps

make prefix=$RPM_BUILD_ROOT%{_prefix} libdir=$RPM_BUILD_ROOT%{_libdir} \
    bindir=$RPM_BUILD_ROOT%{_bindir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} install

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)

%doc AUTHORS FAQ COPYING ChangeLog NEWS README INSTALL TODO BUGS extras 
%{_libdir}/*
%{_bindir}/*
%{_datadir}/pixmaps/hourglass-big.png
%{_datadir}/pixmaps/hourglass-small.png
%{_datadir}/control-center/Xalf/.directory
%{_datadir}/control-center/Xalf/settings.desktop
%{_datadir}/gnome/apps/Settings/Xalf/settings.desktop


%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- macro fixes and cleanups, and removal of the hard-coded paths.

* Mon Feb 12 2001 Peter �strand <astrand@lysator.liu.se>
- version 0.7 (yes, two releases on the same day!)

* Mon Feb 12 2001 Peter �strand <astrand@lysator.liu.se>
- version 0.6

* Wed Jan 31 2001 Peter �strand <astrand@lysator.liu.se>
- version 0.5

* Sun Jun 18 2000 Peter Astrand <altic@lysator.liu.se>
- version 0.4

* Thu Jun 1 2000 Peter Astrand <altic@lysator.liu.se>
- version 0.3

* Sat Apr 15 2000 Peter Astrand <altic@lysator.liu.se>
- version 0.2
