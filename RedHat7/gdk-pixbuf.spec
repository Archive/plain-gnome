# Note that this is NOT a relocatable package
%define  name    gdk-pixbuf
%define  ver     0.9.0
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Name:        %name
Summary:     The GdkPixBuf image handling library
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       System Environment/Libraries
Source:      %{name}-%{ver}.tar.gz
URL:         http://www.gnome.org/
BuildRoot:   /var/tmp/%{name}-root
#Requires:   gtk+ >= 1.2

%description
The GdkPixBuf library provides a number of features, including :

- GdkPixbuf structure for representing images.
- Image loading facilities.
- Rendering of a GdkPixBuf into various formats:
  drawables (windows, pixmaps), GdkRGB buffers.
- Fast scaling and compositing of pixbufs.
- Simple animation loading (ie. animated gifs)

In addition, this module also provides a little libgnomecanvaspixbuf
library, which contains a GNOME Canvas item to display pixbufs with
full affine transformations.

%package devel
Summary:    Libraries and include files for developing GdkPixBuf applications.
Group:      Development/Libraries
Requires:   %name = %ver
#Obsoletes: %name-devel

%description devel
Libraries and include files for developing GdkPixBuf applications.

%prep
%setup -q

%build

%ifarch alpha
    MYARCH_FLAGS="--host=alpha-redhat-linux"
%endif

# Needed for snapshot releases.
MYCFLAGS="$RPM_OPT_FLAGS"

if [ ! -f configure ]; then
  CFLAGS="$MYCFLAGS" ./autogen.sh $MYARCH_FLAGS \
    --localstatedir=/var/lib --prefix=%{_prefix} \
    --sysconfdir=%{_sysconfdir} --bindir=%{_bindir} \
    --libdir=%{_libdir} --includedir=%{_includedir} \
    --datadir=%{_datadir}
fi

CFLAGS="$MYCFLAGS" ./configure $MYARCH_FLAGS --localstatedir=/var/lib \
    --prefix=%{_prefix} --sysconfdir=%{_sysconfdir} \
    --bindir=%{_bindir} --libdir=%{_libdir} \
    --includedir=%{_includedir} --datadir=%{_datadir} 

if [ "$SMP" != "" ]; then
  make -j$SMP "MAKE=make -j$SMP"
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} \
    prefix=$RPM_BUILD_ROOT%{_prefix} bindir=$RPM_BUILD_ROOT%{_bindir} \
    libdir=$RPM_BUILD_ROOT%{_libdir} \
    includedir=$RPM_BUILD_ROOT%{_includedir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} \
    localstatedir=$RPM_BUILD_ROOT/var/lib install

%clean
rm -rf $RPM_BUILD_ROOT

%post 
if ! grep %{_libdir} %{_sysconfdir}/ld.so.conf > /dev/null ; then
  echo "%{_libdir}" >> %{_sysconfdir}/ld.so.conf
fi

/sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)

%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README TODO doc/*.txt html
%{_libdir}/lib*.so.*
%{_libdir}/%{name}/loaders/lib*.so*

%files devel
%defattr(-, root, root)

#%doc HACKING MAINTAINERS
%{_bindir}/*
%{_libdir}/lib*.so
%{_libdir}/%{name}/loaders/lib*.a
%{_libdir}/*.a
%{_libdir}/*.sh
%{_includedir}/*
%{_datadir}/aclocal/*
%{_datadir}/gnome/html/*


%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- A few fixups, and better macros

* Sat Jan 22 2000 Ross Golder <rossigee@bigfoot.com>
- Borrowed from gnome-libs to integrate into gdk-pixbuf source tree
