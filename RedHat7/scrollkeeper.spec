Summary:    ScrollKeeper is a cataloging system for documentation on open systems
Name:       scrollkeeper
Version:    0.1.2
Release:    1_plain_0
Source0:    %{name}-%{version}.tar.gz
Copyright:  LGPL
Group:      System Environment/Base
BuildRoot:  %{_tmppath}/%{name}-buildroot
URL:        http://scrollkeeper.sourceforge.net/
Requires:   libxml


%description 
ScrollKeeper is a cataloging system for documentation. It manages
documentation metadata (as specified by the Open Source Metadata
Framework (OMF)) and provides a simple API to allow help browsers to
find, sort, and search the document catalog. It will also be able to
communicate with catalog servers on the Net to search for documents
which are not on the local system.

%prep
%setup

%build
%configure
if [ "$SMP" != "" ]; then
  make -j$SMP "MAKE=make -j$SMP"
else
  make
fi


%install
rm -rf $RPM_BUILD_ROOT
%makeinstall
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/bg
ln -sf /usr/share/scrollkeeper/templates/bg ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/bg
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/de_AT
ln -sf /usr/share/scrollkeeper/templates/de ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/de_AT
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en
ln -sf /usr/share/scrollkeeper/templates/C ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en_AU
ln -sf /usr/share/scrollkeeper/templates/en ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en_AU
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en_GB
ln -sf /usr/share/scrollkeeper/templates/en ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en_GB
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en_SE
ln -sf /usr/share/scrollkeeper/templates/en ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en_SE
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en_UK
ln -sf /usr/share/scrollkeeper/templates/en ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en_UK
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en_US
ln -sf /usr/share/scrollkeeper/templates/en ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/en_US
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_DO
ln -sf /usr/share/scrollkeeper/templates/es ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_DO
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_ES
ln -sf /usr/share/scrollkeeper/templates/es ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_ES
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_GT
ln -sf /usr/share/scrollkeeper/templates/es ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_GT
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_HN
ln -sf /usr/share/scrollkeeper/templates/es ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_HN
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_MX
ln -sf /usr/share/scrollkeeper/templates/es ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_MX
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_PA
ln -sf /usr/share/scrollkeeper/templates/es ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_PA
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_PE
ln -sf /usr/share/scrollkeeper/templates/es ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_PE
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_SV
ln -sf /usr/share/scrollkeeper/templates/es ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/es_SV
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/ja_JP.eucJP
ln -sf /usr/share/scrollkeeper/templates/ja ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/ja_JP.eucJP
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/no
ln -sf /usr/share/scrollkeeper/templates/nb ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/no
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/no_NY
ln -sf /usr/share/scrollkeeper/templates/nn ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/no_NY
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/pt_BR
ln -sf /usr/share/scrollkeeper/templates/pt ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/pt_BR
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/pt_PT
ln -sf /usr/share/scrollkeeper/templates/pt ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/pt_PT
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/sr_YU
ln -sf /usr/share/scrollkeeper/templates/sr ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/sr_YU
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/sv_SE
ln -sf /usr/share/scrollkeeper/templates/sv ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/sv_SE
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/zh_CN
ln -sf /usr/share/scrollkeeper/templates/zh ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/zh_CN
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/zh_CN.GB2312
ln -sf /usr/share/scrollkeeper/templates/zh ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/zh_CN.GB2312
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/zh_TW
ln -sf /usr/share/scrollkeeper/templates/zh ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/zh_TW
rm -rf ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/zh_TW.Big5
ln -sf /usr/share/scrollkeeper/templates/zh ${RPM_BUILD_ROOT}/usr/share/scrollkeeper/templates/zh_TW.Big5

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc COPYING COPYING-DOCS AUTHORS README ChangeLog NEWS INSTALL
%doc -P doc/scrollkeeper_manual/C/*.sgml
%{_datadir}/omf/*
%{_bindir}/*
%{_mandir}/man8/*
%{_datadir}/scrollkeeper
%{_datadir}/locale/*/*

%post
if [ $1 = 1 ]; then
  # There was previously no SK installed.
  # ie. make a new %{_localstatedir}/lib/scrollkeeper.
  mkdir %{_localstatedir}/lib/scrollkeeper
  scrollkeeper-update -p %{_localstatedir}/lib/scrollkeeper
fi
if [ $1 = 2 ]; then
  # There was previously a SK installed.
  #  ie. don't make a new %{_localstatedir}/lib/scrollkeeper.
  # However, version 0.0.4 of SK did not properly create this
  # directory if a previous SK was installed, so just to be sure...
  rm -rf %{_localstatedir}/lib/scrollkeeper
  mkdir %{_localstatedir}/lib/scrollkeeper
  scrollkeeper-update -p %{_localstatedir}/lib/scrollkeeper
fi

%postun
if [ $1 = 0 ]; then
  # SK is being removed, not upgraded.  
  #  ie. erase {localstatedir}/lib/scrollkeeper.
  rm -rf %{_localstatedir}/lib/scrollkeeper
fi
#if [ $1 = 1 ]; then
#  # SK is being upgraded.  Do not erase {localstatedir}/lib/scrollkeeper.
#fi

%changelog
* Tue Feb 15 2001 Dan Mueth <dan@eazel.com>
- added line to include the translations .mo file

* Tue Feb 06 2001 Dan Mueth <dan@eazel.com>
- fixed up pre and post installation scripts

* Tue Feb 06 2001 Laszlo Kovacs <laszlo.kovacs@sun.com>
- added all the locale directories and links for the template
  content list files

* Wed Jan 17 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- converted to scrollkeeper.spec.in

* Sat Dec 16 2000 Laszlo Kovacs <laszlo.kovacs@sun.com>
- help files added

* Fri Dec 8 2000 Laszlo Kovacs <laszlo.kovacs@sun.com>
- various small fixes added

* Thu Dec 7 2000 Laszlo Kovacs <laszlo.kovacs@sun.com>
- fixing localstatedir problem
- adding postinstall and postuninstall scripts

* Tue Dec 5 2000 Gregory Leblanc <gleblanc@cu-portland.edu>
- adding COPYING, AUTHORS, etc
- fixed localstatedir for the OMF files

* Fri Nov 10 2000 Gregory Leblanc <gleblanc@cu-portland.edu>
- Initial spec file created.


