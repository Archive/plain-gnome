%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary: 	A C++ interface for Gnome libs (a GUI library for X).
Name: 		gnomemm
Version: 	1.1.15
Release: 	%rel
Copyright: 	LGPL
Group: 		System Environment/Libraries
Source: 	%{name}-%{version}.tar.gz
Patch0:         gnomemm-conf-sh-location.patch
URL: 		http://gtkmm.sourceforge.net/
BuildRoot: 	/var/tmp/%{name}-%{version}-root
Requires:	gnome-libs, gtkmm
BuildRequires:  gtkmm-devel, libsigc++-devel

%description
This package provides a C++ interface for GnomeUI.  It is a subpackage
of the Gtk-- project.  The interface provides a convenient interface for C++
programmers to create Gnome GUIs with GTK+'s flexible object-oriented 
framework.

%package	devel
Summary: 	Headers for developing programs that will use Gnome--.
Group: 		Development/Libraries
Requires:       %{name}, gnome-libs-devel

%description    devel
This package contains the headers that programmers will need to develop
applications which will use Gnome--, part of Gtk-- the C++ interface to 
the GTK+ (the Gimp ToolKit) GUI library.

%prep
%setup
%patch -p1

%build

# ...hope this can be removed soon
%ifarch alpha
    ARCH_FLAGS="--host=alpha-linux-gnu"
%endif

# Needed for snapshot releases.
if [ ! -f configure ]; then
    CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh $ARCH_FLAGS \
    	--prefix=%{_prefix} \
    	--enable-static \
    	--enable-shared --enable-docs
fi

CFLAGS="$RPM_OPT_FLAGS" ./configure  $ARCH_FLAGS \
    --prefix=%{_prefix} \
    --disable-maintainer-mode \
    --enable-static \
    --enable-shared --enable-docs

if [ "$SMP" != "" ]; then
    make -j$SMP "MAKE=make -j$SMP"
    make
else
    make
fi

%install
if [ -d $RPM_BUILD_ROOT ]; then rm -rf $RPM_BUILD_ROOT; fi
make prefix=$RPM_BUILD_ROOT%{_prefix} \
    sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} \
    localstatedir=$RPM_BUILD_ROOT%{_localstatedir} install

# replace examples.conf by a really simple one
echo 'CXXBUILD = g++ -O2 $< -o $@ `gtkmm-config --cflags --libs` ' \
	> examples/examples.conf

# strip down the docs 
find docs/ \
\( 	-name 'Makefile' -or	\
	-name 'Makefile.in' -or	\
	-name 'Makefile.am' -or	\
	-name '*.m4' -or	\
	-name 'html' -or	\
	-name 'header' -or 	\
	-name '*.h' 		\
\)	-exec rm {} \;

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/*.so.*

%files  devel
%defattr(-, root, root)
%doc examples/ docs/  AUTHORS COPYING ChangeLog INSTALL NEWS README
#{_bindir}/*
%{_includedir}/*.h
%{_includedir}/gnome--
%{_libdir}/*.la
%{_libdir}/*.a
%{_libdir}/*.sh
%{_libdir}/*.so

###########################################################################
%changelog
* Thu Mar 01 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- removed fixed paths, and updated with better macros.

* Thu May 11 2000 Herbert Valerio Riedel <hvr@gnu.org>
- removed lib/gtkmm from files section
- removed empty obsolete tags

* Sun Jan 30 2000 Karl Einar Nelson <kenelson@sourceforge.net>
- adapted from gtk--.spec
