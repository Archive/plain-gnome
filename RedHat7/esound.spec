%define  ver     0.2.22
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     Allows several audio streams to play on a single audio device.
Name:        esound
Version:     %ver
Release:     %rel
Copyright:   GPL
Group:       System Environment/Daemons
Source0:     esound-%{version}.tar.gz
URL:         http://www.tux.org/~ricdude/EsounD.html
BuildRoot:   /var/tmp/esound-%{version}-root

%description
EsounD, the Enlightened Sound Daemon, is a server process that mixes
several audio streams for playback by a single audio device. For
example, if you're listening to music on a CD and you receive a
sound-related event from ICQ, the two applications won't have to
jockey for the use of your sound card.

Install esound if you'd like to let sound applications share your
audio device. You'll also need to install the audiofile package.

%package devel
Summary:    Development files for EsounD applications.
Group:      Development/Libraries
Requires:   esound
Requires:   audiofile-devel

%description devel
The esound-devel Libraries, include files and other resources you can
use to develop EsounD applications.

Install esound-devel if you want to develop EsounD applications.

%prep
%setup -q

%build

CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%{_prefix} \
    --sysconfdir=%{_sysconfdir} --bindir=%{_bindir} \
    --libdir=%{_libdir} --includedir=%{_includedir} \
    --datadir=%{_datadir}

make

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{_prefix} \
    sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} \
    bindir=$RPM_BUILD_ROOT%{_bindir} libdir=$RPM_BUILD_ROOT%{_libdir} \
    includedir=$RPM_BUILD_ROOT%{_includedir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} install


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog docs/esound.sgml docs/html docs/esound.ps
%doc INSTALL NEWS README TIPS TODO
%config %{_sysconfdir}/*
%{_bindir}/esd
%{_bindir}/esdcat
%{_bindir}/esdctl
%{_bindir}/esddsp
%{_bindir}/esdfilt
%{_bindir}/esdloop
%{_bindir}/esdmon
%{_bindir}/esdplay
%{_bindir}/esdrec
%{_bindir}/esdsample
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)

%{_bindir}/esd-config
%{_libdir}/*a
%{_includedir}/*
%{_datadir}/aclocal/*
%{_libdir}/lib*.so

%changelog
* Mon Feb 19 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- updates and fixes

* Tue Apr 4 2000 Elliot Lee <sopwith@redhat.com> 0.2.18-1
- Update to 0.2.18

* Mon Aug 30 1999 Elliot Lee <sopwith@redhat.com> 0.2.13-1
- Update to 0.2.13
- Merge in changes from RHL 6.0 spec file.

* Sat Nov 21 1998 Pablo Saratxaga <srtxg@chanae.alphanet.ch>

- added %{_prefix}/share/aclocal/* to %files devel
- added spanish and french translations for rpm

* Thu Oct 1 1998 Ricdude <ericmit@ix.netcom.com>

- make autoconf do the version updating for us.

* Wed May 13 1998 Michael Fulbright <msf@redhat.com>

- First try at an RPM
