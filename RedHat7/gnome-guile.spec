# Note that this is NOT a relocatable package
%define  nam     gnome-guile
%define  ver     0.20pre2
%define  RELEASE 0_plain_0
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary:     GNOME guile interpreter
Name:        %nam
Version:     %ver
Release:     %rel
Copyright:   LGPL
Group:       X11/Gnome
Source:      %{nam}-%{ver}.tar.gz
Patch0:      gnome-guile-libglade-configure.patch
BuildRoot:   /tmp/%{nam}-root
Obsoletes:   gnome
URL:         http://www.gnome.org

%description
GNOME guile (gnomeg) is a guile interpreter with GTK and GNOME support.
A number of GNOME utilities are written to use gnomeg.

GNOME is the GNU Network Object Model Environment.  That's a fancy
name but really GNOME is a nice GUI desktop environment.  It makes
using your computer easy, powerful, and easy to configure.

%package devel
Summary:    GNOME guile libraries, includes, etc
Group:      X11/Gnome
Requires:   %{name} = %{version}

%description devel
Libraries and header files for GNOME guile development

%prep
%setup
%patch -p0

%build
# Alpha's return all sorts of diffrent things for alpha, this confuses
# libtool so we press the issue.
%ifarch alpha
    MYARCH_FLAGS="--host=alpha-redhat-linux"
%endif

CFLAGS="$RPM_OPT_FLAGS" ./configure $MYARCH_FLAGS --prefix=%{_prefix} \
    --bindir=%{_bindir} --libdir=%{_libdir} \
    --includedir=%{_includedir} --datadir=%{_datadir} --without-gtkhtml

if [ "$SMP" != "" ]; then
  make -j$SMP "MAKE=make -j$SMP"
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

# FIXME: This will only work on systems where guile is installed
#        in /usr.  Hopefully we can get rid of this stuff soon!

make prefix=$RPM_BUILD_ROOT%{_prefix} bindir=%{_bindir} \
    libdir=$RPM_BUILD_ROOT%{_libdir} \
    includedir=$RPM_BUILD_ROOT%{_includedir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} ROOT=$RPM_BUILD_ROOT \
    sitedir=$RPM_BUILD_ROOT/usr/share/guile/site \
    schemedir=$RPM_BUILD_ROOT/usr/share/guile install 

mkdir -p $RPM_BUILD_ROOT/usr/share/guile/toolkits
rm -f $RPM_BUILD_ROOT/usr/share/guile/toolkits/libgtkstubs.so
ln -s /usr/lib/libguilegtk.so \
      $RPM_BUILD_ROOT/usr/share/guile/toolkits/libgtkstubs.so

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%{_libdir}/lib*.so.*

%{_datadir}/guile/gtk/*.scm
%{_datadir}/guile/gtk-1.2/*.scm
%{_datadir}/guile/gnome/*.scm
%{_datadir}/guile/toolkits/libgtkstubs.so
%{_datadir}/guile/site/event-repl.scm

%files devel
%attr(-, root, root) %{_libdir}/lib*.so
%attr(-, root, root) %{_libdir}/*a
%attr(-, root, root) %{_includedir}/*
%attr(-, root, root) %{_datadir}/guile-gtk/*.defs


%changelog
* Thu Feb 22 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- fixed up macros, and removed hard-coded paths.

* Sat Feb 27 1999 Gregory Mclean <gregm@comstar.net>
- fixed this up so it will build on more then one alpha and this file is
  generated.

* Wed May 27 1998 Michael Fulbright <msf@redhat.com>
- modified file list to include %{_datadir}/guile, %{_datadir}/gtk

* Fri Mar 13 1998 Marc Ewing <marc@redhat.com>
- Integrate into gnome-guile CVS source tree
